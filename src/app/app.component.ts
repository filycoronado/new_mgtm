import { Component } from '@angular/core';
import { ApiService } from 'src/app/service/service';
import { JsonPipe } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'star-admin-angular';
  user;
  permisions = [];
  vartest = false;

  constructor(public ApiService: ApiService) {

  }
  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.vartest = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);

      this.getPermisions();
    } else {
      this.vartest = false;
    }
  }


  getPermisions(): void {
    this.ApiService.getPermissions(this.user.id).then(res => {
      if (res.status == "success") {
        this.permisions = res.data;
       // console.log(this.permisions);
      }

    });
  }
}
