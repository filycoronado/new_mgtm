import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-customer-policy-edit',
  templateUrl: './customer-policy-edit.component.html',
  styleUrls: ['./customer-policy-edit.component.scss']
})
export class CustomerPolicyEditComponent implements OnInit {
  id;
  llc_types;
  user;
  dealers;
  companies;
  cashiers;
  payments_types;
  policy_types;
  status;
  sub_status;
  required_dealer_options;

  public policy = {
    id_user: '',
    id_customer: '',
    id_agency: '',
    id_llc: '1',
    id_company: '',
    id_policy_type: '',
    effective_date: '',
    cancelation_date: '',
    expiration_date: '',
    pending_date: '',
    due_date: '',
    status: '',
    policy_number: '',
    membership_number: '',
    id_dealer: '',
    delivery_name: '',
    delivery_seller: '',
    id_sub_status: '',

  }
  registerPolicy: FormGroup;


  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
    }
    this.ApiService.getLLCTypes().then(res => {
      this.llc_types = res;
    });
    this.ApiService.getAvailableDealer().then(res => {
      if (res.status == "success") {
        this.dealers = res.dealers;
      }
    });
    this.ApiService.getAvailableCompaniesByLLC(this.policy.id_llc).then(res => {
      if (res.status == "success") {
        this.companies = res.companies;
      }
    });
    this.ApiService.getAvailableCashiers().then(res => {
      this.cashiers = res;
    });
    this.ApiService.getAvailableStatusPayment().then(res => {
      this.payments_types = res;
    });
    this.ApiService.getAvailableStatusPolicyTypes().then(res => {
      this.policy_types = res;
    });
    this.ApiService.getAvailableStatusPolicy().then(res => {
      this.status = res;
      this.sub_status = res[0].policySubStatuses;
    });

    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getPolicyById(this.id).then(res => {
          if (res.status == "success") {
            this.policy = res.data;
            this.setData();
          }
          this.spinner.hide();
        });
      }
    });




    this.registerPolicy = this.formBuilder.group({
      id_llc: ['1', Validators.required],
      id_company: ['0', [Validators.required, Validators.min(1)]],
      effective_date: ['', Validators.required],
      pending_date: ['', Validators.required],
      cancel_date: ['', Validators.required],
      expiration_date: ['', Validators.required],
      due_date: ['', Validators.required],
      membership_number: [''],
      policy_number: ['', Validators.required],
      status: ['1', [Validators.required, Validators.min(1)]],
      sub_status: ['2', [Validators.required, Validators.min(1)]],
      id_dealer: ['0', Validators.required],
      seller_name: { value: '', disabled: true },
      delivery_name: { value: '', disabled: true },
      id_policy_type: ['1', Validators.required],

    }, {

    });
  }

  getData(): void {
    this.policy.id_llc = this.registerPolicy.get('id_llc').value;
    this.policy.id_policy_type = this.registerPolicy.get('id_policy_type').value;
    this.policy.id_company = this.registerPolicy.get('id_company').value;
    this.policy.policy_number = this.registerPolicy.get('policy_number').value;
    this.policy.effective_date = this.registerPolicy.get('effective_date').value;
    this.policy.membership_number = this.registerPolicy.get('membership_number').value;
    this.policy.cancelation_date = this.registerPolicy.get('cancel_date').value;
    this.policy.expiration_date = this.registerPolicy.get('expiration_date').value;
    this.policy.pending_date = this.registerPolicy.get('pending_date').value;
    this.policy.due_date = this.registerPolicy.get('due_date').value;
    this.policy.status = this.registerPolicy.get('status').value;
    this.policy.id_dealer = this.registerPolicy.get('id_dealer').value;
    this.policy.delivery_seller = this.registerPolicy.get('seller_name').value;
    this.policy.delivery_name = this.registerPolicy.get('delivery_name').value;
    this.policy.id_sub_status = this.registerPolicy.get("sub_status").value;
  }

  setData(): void {
    this.registerPolicy.get('id_llc').setValue(this.policy.id_llc);
    this.registerPolicy.get('id_policy_type').setValue(this.policy.id_policy_type);
    this.registerPolicy.get('id_company').setValue(this.policy.id_company);
    this.registerPolicy.get('policy_number').setValue(this.policy.policy_number);
    this.registerPolicy.get('effective_date').setValue(new Date(this.policy.effective_date));
    this.registerPolicy.get('membership_number').setValue(this.policy.membership_number);
    this.registerPolicy.get('cancel_date').setValue(new Date(this.policy.cancelation_date));
    this.registerPolicy.get('expiration_date').setValue(new Date(this.policy.expiration_date));
    this.registerPolicy.get('pending_date').setValue(new Date(this.policy.pending_date));
    this.registerPolicy.get('due_date').setValue(new Date(this.policy.due_date));
    this.registerPolicy.get('status').setValue(this.policy.status);
    this.registerPolicy.get('id_dealer').setValue(this.policy.id_dealer);
    this.registerPolicy.get('seller_name').setValue(this.policy.delivery_seller);
    this.registerPolicy.get('delivery_name').setValue(this.policy.delivery_name);

    this.loadSubStatus();
    this.registerPolicy.get('sub_status').setValue(this.policy.id_sub_status);
  }


  onSubmiPolicy(): void {
    this.getData();
    // this.policy.id_user = this.customerSaved.id_user;
    //this.policy.id_customer = this.customerSaved.id;
    //his.policy.id_agency = this.customerSaved.id_agency;
    if (this.registerPolicy.invalid) {
      return;
    }

    this.updatePolicy();
  }
  updatePolicy(): void {
    this.spinner.show();
    this.ApiService.updatePolicy(this.policy, this.id).then(res => {
      if (res.status == "success") {
        //this.policy = res.data;
        //this.setData();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }
  loadSubStatus(): void {
    let val = this.registerPolicy.get('status').value;

    this.spinner.show();
    this.ApiService.getAvailableSubStatusPolicy(val).then(res => {
      if (res.status == "success") {
        this.sub_status = res.data;

      } else {
        alert(res.message);
      }
      this.spinner.hide();
    });
  }
  LoadCompanies(): void {
    let id = this.registerPolicy.get('id_llc').value;
    this.registerPolicy.get("id_company").setValue('0');
    this.ApiService.getAvailableCompaniesByLLC(id).then(res => {
      //"status" => "success",
      //"companies" => $list
      if (res.status == "success") {
        this.companies = res.companies;
      }

    });
  }
  SelectDealer(): void {
    let id = this.registerPolicy.get('id_dealer').value;
    if (id > 0) {
      this.registerPolicy.get("seller_name").setValidators([Validators.required]);
      this.registerPolicy.get("delivery_name").setValidators([Validators.required]);
      this.registerPolicy.get("seller_name").enable();
      this.registerPolicy.get("delivery_name").enable();
      this.required_dealer_options = true;
    } else {
      this.registerPolicy.get("seller_name").setValidators(null);
      this.registerPolicy.get("delivery_name").setValidators(null);
      this.registerPolicy.get("seller_name").setValue('');
      this.registerPolicy.get("delivery_name").setValue('');
      this.registerPolicy.get("seller_name").disable();
      this.registerPolicy.get("delivery_name").disable();
      this.required_dealer_options = false;
    }
    this.registerPolicy.updateValueAndValidity();
  }
}
