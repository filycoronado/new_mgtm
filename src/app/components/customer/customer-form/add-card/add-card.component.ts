import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {
  localPermissions;
  user;
  registerCard: FormGroup;
  submitted = false;
  editmode = false;
  id_policy;
  id_row;
  card = {
    id_policy: '',
    id_customer: '',
    card_number: '',
    card_exp: '',
    back_number: '',
    route_number: '',
    account_number: ''
  }

  constructor(public _location: Location, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.registerCard = this.formBuilder.group({
      card_number: ['', Validators.required],
      card_exp: ['', Validators.required],
      back_number: ['', Validators.required],
      route_number: ['', Validators.min(9)],
      account_number: ['', Validators.min(9)],
    }, {
    });


    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;
        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id_row = params.get('id_item');

            if (this.id_row != 0) {
              this.editmode = true;
              this.setData();

            }
            this.id_policy = params.get('id');
            this.card.id_policy = this.id_policy;
            this.ApiService.getPolicyById(this.id_policy).then(res => {
              if (res.status == "success") {
                // this.status = res.data;
                this.card.id_customer = res.data.id_customer;
              }
              this.spinner.hide();
            });
          }
        });
      });

    }
  }
  get f() { return this.registerCard.controls; }


  getData(): void {
    this.card.card_number = this.registerCard.get('card_number').value;
    this.card.card_exp = this.registerCard.get('card_exp').value;
    this.card.back_number = this.registerCard.get('back_number').value;
    this.card.route_number = this.registerCard.get('route_number').value;
    this.card.account_number = this.registerCard.get('account_number').value;
  }
  setData(): void {
    this.spinner.show();
    this.ApiService.getCard(this.id_row).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.card = res.data;
        this.spinner.hide();
        this.registerCard.get('card_number').setValue(this.card.card_number);
        this.registerCard.get('card_exp').setValue(this.card.card_exp);
        this.registerCard.get('back_number').setValue(this.card.back_number);
        this.registerCard.get('route_number').setValue(this.card.route_number);
        this.registerCard.get('account_number').setValue(this.card.account_number);
      }
      //alert(res.message);
      this.spinner.hide();
    });
  }

  onSubmit(): void {
    this.submitted = true;
    //   this.getDataPayment();
    if (this.registerCard.invalid) {
      return;
    }

    this.getData();
    if (this.editmode) {
      this.Update();
    } else {
      this.Save();
    }
  }

  Save(): void {
    this.spinner.show();
    this.ApiService.saveCard(this.card).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.spinner.hide();
        this._location.back();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }

  Update(): void {
    this.spinner.show();
    this.ApiService.updateCard(this.card, this.id_row).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.spinner.hide();
        this._location.back();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }

}
