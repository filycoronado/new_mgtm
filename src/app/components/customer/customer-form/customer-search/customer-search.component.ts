import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-customer-search',
  templateUrl: './customer-search.component.html',
  styleUrls: ['./customer-search.component.scss']
})
export class CustomerSearchComponent implements OnInit {

  searchEmployeeForm: FormGroup;
  search = {
    "name": '',
    "last_name": '',
    "customer_number": '',
    "policy_number": ''
  };
  ListData;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }



  ngOnInit() {
    this.searchEmployeeForm = this.formBuilder.group({
      name: [''],
      last_name: [''],
      customer_number: [''],
      policy_number: [''],
    }, {
    });
  }

  get f() { return this.searchEmployeeForm.controls; }

  getData(): void {
    this.search.name = this.searchEmployeeForm.get('name').value;
    this.search.last_name = this.searchEmployeeForm.get('last_name').value;
    this.search.customer_number = this.searchEmployeeForm.get('customer_number').value;
    this.search.policy_number = this.searchEmployeeForm.get('policy_number').value;
  }
  onSubmitForm(): void {
    this.getData();
    this.spinner.show();
    this.ApiService.getCustomers(this.search).then(res => {
      if (res.status == "success") {
        this.ListData = res.customers;
      } else {
        alert(res.message);
      }

      this.spinner.hide();



    });
  }

}
