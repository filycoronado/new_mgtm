import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-edit-ticekt',
  templateUrl: './edit-ticekt.component.html',
  styleUrls: ['./edit-ticekt.component.scss']
})
export class EditTicektComponent implements OnInit {

  registerPayment: FormGroup;
  user;
  cashiers;
  payments_types;
  agencies;
  id;


  public payment = {
    id_agency: '',
    id_policy: '',
    id_customer: '',
    id_user: '',
    id_user_cashier: '',
    id_dealer: '',
    ins_method: '',
    insurance_amount: '',
    office_amount: '',
    adc_method: '',
    adc_amount: '',
    type_transaction: '',
    is_credit: '',
    create_date: ''
  }


  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {

    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
    }

    this.ApiService.getAgencies().then(res => {
      this.agencies = res;
    });
    this.ApiService.getAvailableStatusPayment().then(res => {
      this.payments_types = res;
    });
    this.ApiService.getAvailableCashiers().then(res => {
      this.cashiers = res;
    });


    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getPaymentById(this.id).then(res => {
          if (res.status == "success") {
            this.payment = res.data;
            this.setData();
          }
          this.spinner.hide();

        });
      }
    });

    this.registerPayment = this.formBuilder.group({
      id_agency: ['1', Validators.required],
      ins_method: ['1', Validators.required],
      insurance_amount: ['0', Validators.required],
      office_amount: ['0', Validators.required],
      adc_method: ['1', Validators.required],
      adc_amount: ['0', Validators.required],
      type_transaction: ['1', Validators.required],
      cashier: ['0', Validators.required],
      create_date: [new Date(), Validators.required]

    }, {

    });
  }
  get C() { return this.registerPayment.controls; }


  getDataPayment(): void {
    this.payment.id_agency = this.registerPayment.get('id_agency').value;
    this.payment.ins_method = this.registerPayment.get('ins_method').value;
    this.payment.insurance_amount = this.registerPayment.get('insurance_amount').value;
    this.payment.office_amount = this.registerPayment.get('office_amount').value;
    this.payment.adc_method = this.registerPayment.get('adc_method').value;
    this.payment.adc_amount = this.registerPayment.get('adc_amount').value;
    this.payment.id_user_cashier = this.registerPayment.get('cashier').value;
    this.payment.type_transaction = this.registerPayment.get('type_transaction').value;
    this.payment.create_date = this.registerPayment.get('create_date').value;
  }
  setData(): void {
    if (this.payment.id_user_cashier == null) {
      this.payment.id_user_cashier = '0';
    }
    this.registerPayment.get('id_agency').setValue(this.payment.id_agency);
    this.registerPayment.get('ins_method').setValue(this.payment.ins_method);
    this.registerPayment.get('insurance_amount').setValue(this.payment.insurance_amount);
    this.registerPayment.get('office_amount').setValue(this.payment.office_amount);
    this.registerPayment.get('adc_method').setValue(this.payment.adc_method);
    this.registerPayment.get('adc_amount').setValue(this.payment.adc_amount);
    this.registerPayment.get('cashier').setValue(this.payment.id_user_cashier);
    this.registerPayment.get('type_transaction').setValue(this.payment.type_transaction);
    this.registerPayment.get('create_date').setValue(new Date(this.payment.create_date));

  }

  onSubmitPayment(): void {
    this.getDataPayment();
    if (this.registerPayment.invalid) {
      return;
    }
    this.savePayment();
  }

  savePayment(): void {
    this.spinner.show();
    this.ApiService.updatePayment(this.payment, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      });
    });
  }

}
