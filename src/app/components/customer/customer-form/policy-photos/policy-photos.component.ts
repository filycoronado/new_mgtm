import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GlobalService } from 'src/app/service/global.service';

@Component({
  selector: 'app-policy-photos',
  templateUrl: './policy-photos.component.html',
  styleUrls: ['./policy-photos.component.scss']
})
export class PolicyPhotosComponent implements OnInit {

  id;
  id_customer;
  files;

  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute, private globalService: GlobalService) {

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getPhotosByPolicy(this.id).then(res => {
          if (res.status === "success") {
            this.files = res.data;
          } else {
            alert(res.message);
          }
          this.spinner.hide();
        });


        this.ApiService.getPolicyById(this.id).then(res => {
          if (res.status == "success") {
           // this.status = res.data;
           this.id_customer=res.data.id_customer;
          }
          this.spinner.hide();
        });

        
      }else{
        alert("errro param");
      }
    });
  }

  download(name) {
    return this.globalService.FilesUrl + name;
  }
}
