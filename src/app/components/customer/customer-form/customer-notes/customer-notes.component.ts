import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GlobalService } from 'src/app/service/global.service';
@Component({
  selector: 'app-customer-notes',
  templateUrl: './customer-notes.component.html',
  styleUrls: ['./customer-notes.component.scss']
})
export class CustomerNotesComponent implements OnInit {
  id;
  notes;
  id_customer;
  types;
  user;
  localPermissions;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute, private globalService: GlobalService) {

  }

  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;

        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id = params.get('id');
            this.spinner.show();
            this.ApiService.getPolicyById(this.id).then(res => {
              if (res.status == "success") {
                // this.status = res.data;
                this.id_customer = res.data.id_customer;
              }
              this.spinner.hide();
            });
            this.spinner.show();
            this.ApiService.Get_notes_by_policy(this.id).then(res => {
              if (res.status == "success") {
                this.notes = res.data;
              }
              alert(res.message);
              this.spinner.hide();
            });

          }
        });

      });

    }
  }

  getStatus(item): string {
    let strd = "";
    if (item == 0) {
      strd = "Incomplete";

    } else {
      strd = "Complete";
    }

    return strd;
  }

  isDisabled(id, ida): boolean {
    if (id == ida) {
      return false
    } else {
      return true;
    }
  }

  changeStatus(item): void {
    if (confirm("Are you sure to change ")) {
      this.spinner.show();
      this.ApiService.change_statusNote(item.id, item.status_note).then(res => {
        if (res.status == "success") {
          if (item.status_note == 0) {
            item.status_note = 1;

          } else {
            item.status_note = 0;
          }
        }
        alert(res.message);
        this.spinner.hide();
        // location.reload();
      });
    }
  }

}
