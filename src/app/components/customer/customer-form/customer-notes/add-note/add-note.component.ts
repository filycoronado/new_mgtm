import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Location } from '@angular/common';
@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {

  user;
  registerAgencyForm: FormGroup;
  submitted = false;
  employees;
  id_customer;
  id;
  edtimode = false;
  id_row;
  private note = {
    note: '',
    id_policy: '',
    id_customer: '',
    id_user_asigned: '',
    id_note_type: '',
    follow_date: '',
    id_user: ''
  };
  types;
  public Editor = ClassicEditor;
  constructor(private _location: Location, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
    }
    this.spinner.show();
    this.ApiService.Get_types().then(res => {
      this.types = res;
      this.spinner.hide();
    });
    this.ApiService.getAvailableUsers().then(res => {

      if (res.status = "success") {
        this.employees = res.employees;
      }

      this.spinner.hide();
    });




    this.registerAgencyForm = this.formBuilder.group({
      id_note_type: ['1', Validators.required],
      note: ['<b>P:</b> <br/> <br/>  <b> C: </b> <br/> <br/> <b>A:</b>', Validators.required],
      follow_date: [new Date(), Validators.required],
      id_user_asigned: ['0'],
    }, {

    });


    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getPolicyById(this.id).then(res => {
          if (res.status == "success") {
            // this.status = res.data;
            this.id_customer = res.data.id_customer;
            // console.log(this.id_customer);
          }
          this.spinner.hide();
        });


        if (params.get('id_item') != null && params.get('id_item') != '0') {
          this.id_row = params.get('id_item');
          this.edtimode = true;
          // alert("edit mode" + this.id_row);
          this.getItem();
        }

      }
    });
  }
  // convenience getter for easy access to form fields
  get C() { return this.registerAgencyForm.controls; }


  getData(): void {
    this.note.id_user = this.user.id;
    this.note.id_policy = this.id;
    this.note.id_user_asigned = this.registerAgencyForm.get('id_user_asigned').value;
    this.note.note = this.registerAgencyForm.get('note').value;
    this.note.id_customer = this.id_customer;
    this.note.follow_date = this.registerAgencyForm.get('follow_date').value;
    this.note.id_note_type = this.registerAgencyForm.get('id_note_type').value;

  }
  getItem(): void {

    this.spinner.show();
    this.ApiService.getNoteById(this.id_row).then(res => {
      if (res.status == "success") {
        this.note = res.data;
        this.setData();
      }
      this.spinner.hide();
    });
  }

  setData(): void {

    this.registerAgencyForm.get("id_user_asigned").setValue(this.note.id_user_asigned);
    this.registerAgencyForm.get("note").setValue(this.note.note);
    this.registerAgencyForm.get("follow_date").setValue(new Date(this.note.follow_date));
    this.registerAgencyForm.get("id_note_type").setValue(this.note.id_note_type);

  }
  onSubmit(): void {
    if (this.registerAgencyForm.invalid) {
      return;
    }
    if (!this.edtimode) {
      this.savenote();
    } else {
      this.updatenote();
    }
  }

  savenote(): void {
    this.getData();
    this.spinner.show();
    this.ApiService.save_note_policy(this.note).then(res => {
      if (res.status == "success") {
        alert(res.message);

        this._location.back();
        this.spinner.hide();
      }
      this.spinner.hide();

    });
  }


  updatenote(): void {
    this.getData();
    this.spinner.show();
    this.ApiService.update_note_policy(this.note, this.id_row).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this._location.back();
        this.spinner.hide();
      }
      this.spinner.hide();

    });
  }

}
