import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GlobalService } from 'src/app/service/global.service';
@Component({
  selector: 'app-customer-files',
  templateUrl: './customer-files.component.html',
  styleUrls: ['./customer-files.component.scss']
})
export class CustomerFilesComponent implements OnInit {
  id;
  files;
  id_customer;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute, private globalService: GlobalService) {

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getFilesByPolicy(this.id).then(res => {
          if (res.status === "success") {
            this.files = res.data;
          } else {
            alert(res.message);
          }
          this.spinner.hide();
        });


        this.ApiService.getPolicyById(this.id).then(res => {
          if (res.status == "success") {
           // this.status = res.data;
           this.id_customer=res.data.id_customer;
          }
          this.spinner.hide();
        });
      }
    });
  }

  download(name) {
    return this.globalService.FilesUrl + name;
  }

}
