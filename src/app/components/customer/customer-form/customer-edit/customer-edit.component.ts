import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {
  id;
  user;
  registerEmployeeForm: FormGroup;


  public customer = {
    id_agency: '',
    id_user: '',
    name: '',
    last_name: '',
    address: '',
    state: '',
    zipcode: '',
    email: '',
    phone: '',
    work_phone: '',
    home_phone: '',

  };
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);

    } else {

    }


    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getCustomerById(this.id).then(res => {
          if (res.status == "success") {
            this.customer = res.data;
            this.setData();
          }
          this.spinner.hide();
        });
      }
    });


    this.registerEmployeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      zipcode: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      work_phone: [''],
      home_phone: [''],

    }, {

    });
  }

  get f() { return this.registerEmployeeForm.controls; }


  getData(): void {
    this.customer.name = this.registerEmployeeForm.get('name').value;
    this.customer.last_name = this.registerEmployeeForm.get('last_name').value;
    this.customer.address = this.registerEmployeeForm.get('address').value;
    this.customer.state = this.registerEmployeeForm.get('state').value;
    this.customer.zipcode = this.registerEmployeeForm.get('zipcode').value;
    this.customer.email = this.registerEmployeeForm.get('email').value;
    this.customer.phone = this.registerEmployeeForm.get('phone').value;
    this.customer.home_phone = this.registerEmployeeForm.get('home_phone').value;
    this.customer.work_phone = this.registerEmployeeForm.get('work_phone').value;
  }
  setData(): void {
    this.registerEmployeeForm.get('name').setValue(this.customer.name);
    this.registerEmployeeForm.get('last_name').setValue(this.customer.last_name);
    this.registerEmployeeForm.get('address').setValue(this.customer.address);
    this.registerEmployeeForm.get('state').setValue(this.customer.state);
    this.registerEmployeeForm.get('zipcode').setValue(this.customer.zipcode);
    this.registerEmployeeForm.get('email').setValue(this.customer.email);
    this.registerEmployeeForm.get('phone').setValue(this.customer.phone);
    this.registerEmployeeForm.get('home_phone').setValue(this.customer.home_phone);
    this.registerEmployeeForm.get('work_phone').setValue(this.customer.work_phone);
  }

  onSubmitEmployee(): void {
    this.getData();
    // this.customer.id_user = this.user.id;
    //this.customer.id_agency = this.user.id_agency;
    //  this.submitted = true;
    // stop here if form is invalid
    if (this.registerEmployeeForm.invalid) {
      return;
    }
    // display form values on success
    this.updateCustomer();


  }

  updateCustomer(): void {
    this.spinner.show();
    this.ApiService.updateCustomer(this.customer, this.id).then(res => {
      if (res.status == "success") {
        //  this.customer = res.data;
        //this.setData();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }
}

