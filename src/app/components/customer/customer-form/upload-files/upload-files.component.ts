import { Component, OnInit, NgModule, Inject, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from "@angular/common/http"

import { Subscription } from 'rxjs'

import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { GlobalService } from 'src/app/service/global.service';
import { Location } from '@angular/common';

import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {

  @ViewChild('attachments', { static: false }) attachment: any;
  postUrl = '...'
  buttonText;
  myFormData: FormData//populated by ngfFormData directive
  httpEvent: HttpEvent<{}>
  files;
  types;
  id;
  user;
  selectedFile = {
    'file': File,
    'type': '1',
  }
  fileList: File[] = [];
  listOfFiles: any[] = [];
  typeFiles;
  constructor(private _location: Location, public HttpClient: HttpClient, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute, public globalService: GlobalService, ) { }

  ngOnInit() {

    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
    }
    this.ApiService.getTypesFiles().then(res => {
      if (res.status == "success") {
        this.types = res.data;
      }
    });
    this.route.paramMap.subscribe(params => {

      if (params.get('type') != null) {
        this.typeFiles = params.get('type');
        if (this.typeFiles == 1) {
          this.buttonText = "Upload Photos";
        } else {
          this.buttonText = "Upload Files";
        }

      }
      if (params.get('id') != null) {
        this.id = params.get('id');
      }
    });
  }

  SubmitUpload(): void {
    if (this.typeFiles == 1) {
      this.uploadPhotos();
    } else {
      this.uploadFiles();
    }
  }
  uploadFiles(): Subscription {
    const formData: FormData = new FormData();
    for (var i = 0; i <= this.listOfFiles.length - 1; i++) {
      formData.append('file' + i, this.listOfFiles[i].file, this.listOfFiles[i].file.name);
      formData.append('type' + i, this.listOfFiles[i].type);
    }
    formData.append('length', this.listOfFiles.length - 1 + '');
    formData.append('id_policy', this.id);
    formData.append('id_user', this.user.id);

    const config = new HttpRequest('POST', this.globalService.serverUrl + 'file/save', formData, {
      reportProgress: true
    });
    return this.HttpClient.request(config)
      .subscribe(event => {
        this.httpEvent = event

        if (event instanceof HttpResponse) {
          alert('upload complete');
          this._location.back();
        }
      },
        error => {
          alert('!failure beyond compare cause:' + error.toString())
        })
  }

  uploadPhotos(): Subscription {
    const formData: FormData = new FormData();
    for (var i = 0; i <= this.listOfFiles.length - 1; i++) {
      formData.append('file' + i, this.listOfFiles[i].file, this.listOfFiles[i].file.name);
      formData.append('type' + i, this.listOfFiles[i].type);
    }
    formData.append('length', this.listOfFiles.length - 1 + '');
    formData.append('id_policy', this.id);
    formData.append('id_user', this.user.id);

    const config = new HttpRequest('POST', this.globalService.serverUrl + 'file/save_photos', formData, {
      reportProgress: true
    });
    return this.HttpClient.request(config)
      .subscribe(event => {
        this.httpEvent = event

        if (event instanceof HttpResponse) {
          alert('upload complete');
          this._location.back();
        }
      },
        error => {
          alert('!failure beyond compare cause:' + error.toString())
        })
  }

  onFileChanged(event: any) {
    for (var i = 0; i <= event.target.files.length - 1; i++) {
      var selectedFile = {
        'file': event.target.files[i],
        'type': '1',
      }
      this.fileList.push(selectedFile.file);
      this.listOfFiles.push(selectedFile)
    }

    this.attachment.nativeElement.value = '';
  }



  removeSelectedFile(index) {
    // Delete the item from fileNames list
    this.listOfFiles.splice(index, 1);
    // delete file from FileList
    this.fileList.splice(index, 1);
  }

}
