import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit {
  customerSaved;
  policySaved;
  user;
  payments_types;
  policy_types;
  myDateValue: Date;
  cancelDate: Date;
  expDate: Date;
  due_date: Date;
  pending_date: Date;
  cashiers;

  id;
  edit = false;
  title = "Create Customer";
  registerEmployeeForm: FormGroup;
  registerPolicy: FormGroup;
  registerPayment: FormGroup;
  submitted = false;
  submittedPolicyForm = false;
  required_dealer_options = false;
  customerInfo = false;
  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };


  companies;
  llc_types;
  dealers;
  status;
  sub_status;
  public customer = {
    id_agency: '',
    id_user: '',
    name: '',
    last_name: '',
    address: '',
    state: '',
    zipcode: '',
    email: '',
    phone: '',
    work_phone: '',
    home_phone: '',

  };
  public policy = {
    id_user: '',
    id_customer: '',
    id_agency: '',
    id_llc: '1',
    id_company: '',
    id_policy_type: '',
    effective_date: '',
    cancelation_date: '',
    expiration_date: '',
    pending_date: '',
    due_date: '',
    status: '',
    policy_number: '',
    membership_number: '',
    id_dealer: '',
    delivery_name: '',
    delivery_seller: '',
    id_sub_status: '',

  }
  public payment = {
    id_policy: '',
    id_customer: '',
    id_user: '',
    id_user_cashier: '',
    id_dealer: '',
    ins_method: '',
    insurance_amount: '',
    office_amount: '',
    adc_method: '',
    adc_amount: '',
    type_transaction: '',
    is_credit: '',

  }
  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }
  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);

    } else {

    }

    this.myDateValue = new Date();
    var dias = parseInt('30');
    var dias2 = parseInt('180');
    //nueva fecha sumada
    this.cancelDate = new Date();
    this.cancelDate.setDate(this.myDateValue.getDate() + dias);

    this.expDate = new Date();
    this.expDate.setDate(this.myDateValue.getDate() + dias2);

    this.due_date = new Date();
    this.due_date.setDate(this.myDateValue.getDate() + dias);

    this.pending_date = new Date();
    this.pending_date.setDate(this.myDateValue.getDate() + 37);
    this.ApiService.getLLCTypes().then(res => {
      this.llc_types = res;
    });
    this.ApiService.getAvailableDealer().then(res => {
      if (res.status == "success") {
        this.dealers = res.dealers;
      }
    });


    this.ApiService.getAvailableCompaniesByLLC(this.policy.id_llc).then(res => {
      if (res.status == "success") {
        this.companies = res.companies;
      }
    });

    this.ApiService.getAvailableCashiers().then(res => {
      this.cashiers = res;
    });


    this.ApiService.getAvailableStatusPayment().then(res => {

      this.payments_types = res;

    });

    this.ApiService.getAvailableStatusPolicyTypes().then(res => {

      this.policy_types = res;

    });
    this.ApiService.getAvailableStatusPolicy().then(res => {
      this.status = res;
      this.sub_status = res[0].policySubStatuses;
    });

    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getAgencyById(this.id).then(res => {
          if (res.status == "success") {
            this.customer = res.data;
            this.setData();
            this.edit = true;
            this.title = "Update Agency";
          }

          this.spinner.hide();

        });
      }

    });

    this.registerEmployeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      zipcode: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      work_phone: [''],
      home_phone: [''],

    }, {

    });

    this.registerPolicy = this.formBuilder.group({
      id_llc: ['1', Validators.required],
      id_company: ['0', [Validators.required, Validators.min(1)]],
      effective_date: [this.myDateValue, Validators.required],
      pending_date: [this.pending_date, Validators.required],
      cancel_date: [this.cancelDate, Validators.required],
      expiration_date: [this.expDate, Validators.required],
      due_date: [this.due_date, Validators.required],
      membership_number: [''],
      policy_number: ['', Validators.required],
      status: ['1', [Validators.required, Validators.min(1)]],
      sub_status: ['2', [Validators.required, Validators.min(1)]],
      id_dealer: ['0', Validators.required],
      seller_name: { value: '', disabled: true },
      delivery_name: { value: '', disabled: true },
      id_policy_type: ['1', Validators.required],

    }, {

    });

    this.registerPayment = this.formBuilder.group({
      ins_method: ['1', Validators.required],
      insurance_amount: ['0', Validators.required],
      office_amount: ['0', Validators.required],
      adc_method: ['1', Validators.required],
      adc_amount: ['0', Validators.required],
      type_transaction: ['1', Validators.required],
      cashier: ['0', Validators.required]

    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerEmployeeForm.controls; }

  get p() { return this.registerPolicy.controls; }
  get C() { return this.registerPayment.controls; }


  getData(): void {
    this.customer.name = this.registerEmployeeForm.get('name').value;
    this.customer.last_name = this.registerEmployeeForm.get('last_name').value;
    this.customer.address = this.registerEmployeeForm.get('address').value;
    this.customer.state = this.registerEmployeeForm.get('state').value;
    this.customer.zipcode = this.registerEmployeeForm.get('zipcode').value;
    this.customer.email = this.registerEmployeeForm.get('email').value;
    this.customer.phone = this.registerEmployeeForm.get('phone').value;
    this.customer.home_phone = this.registerEmployeeForm.get('home_phone').value;
    this.customer.work_phone = this.registerEmployeeForm.get('work_phone').value;

  }
  getDataPolicy(): void {
    this.policy.id_llc = this.registerPolicy.get('id_llc').value;
    this.policy.id_policy_type = this.registerPolicy.get('id_policy_type').value;
    this.policy.id_company = this.registerPolicy.get('id_company').value;
    this.policy.policy_number = this.registerPolicy.get('policy_number').value;
    this.policy.effective_date = this.registerPolicy.get('effective_date').value;
    this.policy.membership_number = this.registerPolicy.get('membership_number').value;
    this.policy.cancelation_date = this.registerPolicy.get('cancel_date').value;
    this.policy.expiration_date = this.registerPolicy.get('expiration_date').value;
    this.policy.pending_date = this.registerPolicy.get('pending_date').value;
    this.policy.due_date = this.registerPolicy.get('due_date').value;
    this.policy.status = this.registerPolicy.get('status').value;
    this.policy.id_dealer = this.registerPolicy.get('id_dealer').value;
    this.policy.delivery_seller = this.registerPolicy.get('seller_name').value;
    this.policy.delivery_name = this.registerPolicy.get('delivery_name').value;
    this.policy.id_sub_status = this.registerPolicy.get("sub_status").value;

  }
  getDataPayment(): void {
    this.payment.ins_method = this.registerPayment.get('ins_method').value;
    this.payment.insurance_amount = this.registerPayment.get('insurance_amount').value;
    this.payment.office_amount = this.registerPayment.get('office_amount').value;
    this.payment.adc_method = this.registerPayment.get('adc_method').value;
    this.payment.adc_amount = this.registerPayment.get('adc_amount').value;
    this.payment.id_user_cashier = this.registerPayment.get('cashier').value;
    this.payment.type_transaction = this.registerPayment.get('type_transaction').value;
  }
  setData(): void {
    this.registerEmployeeForm.get("name").setValue(this.customer.name);
    this.registerEmployeeForm.get("last_name").setValue(this.customer.last_name);
    this.registerEmployeeForm.get("address").setValue(this.customer.address);
    this.registerEmployeeForm.get("state").setValue(this.customer.state);
    this.registerEmployeeForm.get("zipcode").setValue(this.customer.zipcode);
    this.registerEmployeeForm.get("email").setValue(this.customer.email);
    this.registerEmployeeForm.get("phone").setValue(this.customer.email);
    this.registerEmployeeForm.get("home_phone").setValue(this.customer.email);
    this.registerEmployeeForm.get("work_phone").setValue(this.customer.email);

  }

  onSubmitEmployee(): void {
    this.getData();
    this.customer.id_user = this.user.id;
    this.customer.id_agency = this.user.id_agency;
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerEmployeeForm.invalid) {
      return;
    }
    // display form values on success
    this.saveCustomer();


  }
  private updateCustomer(): void {
    this.spinner.show();
    this.ApiService.updateAgency(this.customer, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private saveCustomer(): void {
    this.spinner.show();
    this.ApiService.saveCustomer(this.customer).then(res => {
      setTimeout(() => {
        this.customerInfo = true;
        document.getElementById("nav-policy-tab").click();

        if (res.status == "success") {
          //this.reset();
          this.customerSaved = res.customer;
          alert(res.message);

        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private reset(): void {
    this.customer = {
      id_agency: '',
      id_user: '',
      name: '',
      last_name: '',
      address: '',
      state: '',
      zipcode: '',
      email: '',
      phone: '',
      work_phone: '',
      home_phone: '',

    };
    this.registerEmployeeForm = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      zipcode: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      work_phone: [''],
      home_phone: [''],

    }, {

    });
  }


  LoadCompanies(): void {
    let id = this.registerPolicy.get('id_llc').value;
    this.registerPolicy.get("id_company").setValue('0');
    this.ApiService.getAvailableCompaniesByLLC(id).then(res => {
      //"status" => "success",
      //"companies" => $list
      if (res.status == "success") {
        this.companies = res.companies;
      }

    });
  }
  SelectDealer(): void {

    let id = this.registerPolicy.get('id_dealer').value;
    if (id > 0) {
      this.registerPolicy.get("seller_name").setValidators([Validators.required]);
      this.registerPolicy.get("delivery_name").setValidators([Validators.required]);
      this.registerPolicy.get("seller_name").enable();
      this.registerPolicy.get("delivery_name").enable();
      this.required_dealer_options = true;
    } else {
      this.registerPolicy.get("seller_name").setValidators(null);
      this.registerPolicy.get("delivery_name").setValidators(null);
      this.registerPolicy.get("seller_name").setValue('');
      this.registerPolicy.get("delivery_name").setValue('');
      this.registerPolicy.get("seller_name").disable();
      this.registerPolicy.get("delivery_name").disable();
      this.required_dealer_options = false;

    }
    this.registerPolicy.updateValueAndValidity();
  }

  ValidateCashier(): void {
    let value = this.registerPayment.get('ins_method').value;
    let value2 = this.registerPayment.get('adc_method').value;
    let flag = false;
    if (value == '1' || value2 == '1') {
      flag = true;
    }
    if (!flag) {

      this.registerPayment.get("cashier").setValidators(null);
      this.registerPayment.get("cashier").setValue('0');
      this.registerPayment.get("cashier").disable();
    } else {
      this.registerPayment.get("cashier").setValidators([Validators.required]);
      this.registerPayment.get("cashier").enable();
    }
    this.registerPayment.updateValueAndValidity();
  }

  onDateChange(newDate: Date) {

  }

  onSubmiPolicy(): void {
    this.getDataPolicy();
    this.policy.id_user = this.customerSaved.id_user;
    this.policy.id_customer = this.customerSaved.id;
    this.policy.id_agency = this.customerSaved.id_agency;
    if (this.registerPolicy.invalid) {
      return;
    }

    this.savePolicy();
  }

  private savePolicy(): void {
    this.spinner.show();
    this.ApiService.savePolicy(this.policy).then(res => {
      setTimeout(() => {
        this.submittedPolicyForm = true;
        if (res.status == "success") {
          //this.reset();
          this.policySaved = res.policy;
          alert(res.message);
          document.getElementById("nav-contact-tab").click();

        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }

  private savePayment(): void {
    this.spinner.show();
    this.ApiService.savePayment(this.payment).then(res => {
      setTimeout(() => {
        this.submittedPolicyForm = true;
        if (res.status == "success") {
          alert(res.message);
          // document.getElementById("nav-contact-tab").click();

        } else {
          alert(res.message);
        }
        this.spinner.hide();
      });
    });
  }
  private onSubmitPayment(): void {
    this.getDataPayment();
    this.payment.id_policy = this.policySaved.id;
    this.payment.id_customer = this.customerSaved.id;
    this.payment.id_user = this.policySaved.id_user;
    this.payment.id_dealer = this.policySaved.id_dealer;

    if (this.registerPayment.invalid) {
      return;
    }
    this.savePayment();
  }

}
