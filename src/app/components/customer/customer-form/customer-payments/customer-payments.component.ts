import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-customer-payments',
  templateUrl: './customer-payments.component.html',
  styleUrls: ['./customer-payments.component.scss']
})
export class CustomerPaymentsComponent implements OnInit {
  id;
  id_customer;
  payments;
  id_selected;
  user;
  localPermissions;
  url: string = "http://localhost/new_mgtm/Back/web/index.php?r=ticket/render_ticket&id=";
  urlSafe: SafeResourceUrl;


  constructor(public sanitizer: DomSanitizer, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {

    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;

        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id = params.get('id');
            this.spinner.show();
            this.ApiService.getPayments(this.id).then(res => {
              if (res.status === "success") {
                this.payments = res.data;
                this.id_customer = res.data[0].id_customer;
                console.log(res.data);
              } else {
                alert(res.message);
              }
              this.spinner.hide();
            });
          }
        });

      });
    }
  }

  getSum(ins_amount, office, adc): any {
    ins_amount = parseFloat(ins_amount);
    office = parseFloat(office);
    adc = parseFloat(adc);
    //console.log(ins_amount+office+adc);
    return (ins_amount + office + adc);
  }

  removeItem(item): void {
    if (confirm("Are you sure to delete this ticket?")) {
      console.log("Implement delete functionality here");
    }
  }

  selectTicekt(id): void {
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url + id);
  }

}
