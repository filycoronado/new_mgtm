import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GlobalService } from 'src/app/service/global.service';
import { MaskApplierService } from 'ngx-mask';


@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {
  id;
  notes;
  id_customer;
  types;
  user;
  localPermissions;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute, private globalService: GlobalService) {

  }

  ngOnInit() {


    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;

        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id = params.get('id');
            this.spinner.show();
            this.ApiService.getPolicyById(this.id).then(res => {
              if (res.status == "success") {
                // this.status = res.data;
                this.id_customer = res.data.id_customer;
              }
              this.spinner.hide();
            });
            this.spinner.show();
            this.ApiService.Get_bank_by_policy(this.id).then(res => {
              if (res.status == "success") {
                this.notes = res.data;
              }
              alert(res.message);
              this.spinner.hide();
            });

          }
        });

      });

    }
  }

  isDisabled(id, ida): boolean {
    if (id == ida) {
      return false
    } else {
      return true;
    }
  }
  getStatus(item): string {
    let strd = "";
    if (item == 0) {
      strd = "Incomplete";

    } else {
      strd = "Complete";
    }

    return strd;
  }

  changeStatus(item): void {
    if (confirm("Are you sure to change ")) {
      this.spinner.show();
      this.ApiService.change_statusNote(item.id, item.status_note).then(res => {
        if (res.status == "success") {
          if (item.status_note == 0) {
            item.status_note = 1;

          } else {
            item.status_note = 0;
          }
        }
        alert(res.message);
        this.spinner.hide();
        // location.reload();
      });
    }
  }

  formarCard(string): string {
    let cadena1 = string.substring(0, 4);
    let cadena2 = string.substring(4, 8);
    let cadena3 = string.substring(8, 12);
    let cadena4 = string.substring(12, 16);

    if (this.localPermissions.view_cc == 1) {

      return cadena1 + '-' + cadena2 + '-' + cadena3 + '-' + cadena4;
    }

    return "XXXX-XXXX-XXXX-" + cadena4;
  }

}
