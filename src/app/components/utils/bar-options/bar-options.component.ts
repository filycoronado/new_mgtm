import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bar-options',
  templateUrl: './bar-options.component.html',
  styleUrls: ['./bar-options.component.scss']
})
export class BarOptionsComponent implements OnInit {
  @Input() public urlCustomer: string;
  @Input() public urlPayments: string;
  @Input() public urlFiles: string;
  @Input() public urlPhotos: string;
  @Input() public urlNotes: string;
  @Input() public urlUpload: string;
  @Input() public UploadText: string;
  @Input() public urlStatus: string;
  @Input() public urlBank: string;
  @Input() public urlVins: string;
  @Input() public UploadVisible: boolean;
  @Input() public ShowBtn: boolean;
  constructor() { }

  ngOnInit() {
  }

}
