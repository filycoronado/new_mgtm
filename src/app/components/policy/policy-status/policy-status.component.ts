import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-policy-status',
  templateUrl: './policy-status.component.html',
  styleUrls: ['./policy-status.component.scss']
})
export class PolicyStatusComponent implements OnInit {

  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }
  id;
  id_customer;
  status = [];
  user;
  localPermissions;
  FlagBtn = false;
  data = {
    'id_policy': ''
  };
  ngOnInit() {

    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;

        if (this.localPermissions.add_status == 1) {
          this.FlagBtn = true;
        } else {
          this.FlagBtn = false;
        }


        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id = params.get('id');

            this.data.id_policy = this.id;

            this.ApiService.getPolicyById(this.id).then(res => {
              if (res.status == "success") {
                // this.status = res.data;
                this.id_customer = res.data.id_customer;
              }
              this.spinner.hide();
            });
          }
        });

        if (this.id != null) {
          this.spinner.show();
          this.ApiService.getRequermientsPolicy(this.data).then(res => {
            if (res.status == "success") {
              this.status = res.data;
            }
            this.spinner.hide();
          });
        }


      });

    }


  }

}
