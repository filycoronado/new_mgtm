import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-payment-policy',
  templateUrl: './payment-policy.component.html',
  styleUrls: ['./payment-policy.component.scss']
})
export class PaymentPolicyComponent implements OnInit {

  registerPayment: FormGroup;
  submittedPolicyForm = false;
  cashiers;
  payments_types;
  id_customer;
  user;
  id;
  public payment = {
    id_policy: '',
    id_customer: '',
    id_user: '',
    id_user_cashier: '',
    id_dealer: '',
    ins_method: '',
    insurance_amount: '',
    office_amount: '',
    adc_method: '',
    adc_amount: '',
    type_transaction: '',
    is_credit: '',
    is_endrose: '',
    concept: '',
    id_agency: '',

  }


  constructor(public _location: Location, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.registerPayment = this.formBuilder.group({
      ins_method: ['1', Validators.required],
      insurance_amount: ['0', Validators.required],
      office_amount: ['0', Validators.required],
      adc_method: ['1', Validators.required],
      adc_amount: ['0', Validators.required],
      type_transaction: ['1', Validators.required],
      cashier: ['0', [Validators.required, Validators.min(1)]],
      is_endrose: ['0', Validators.required],
      concept: [''],


    }, {

    });


    this.registerPayment.get("concept").disable();

    this.ApiService.getAvailableCashiers().then(res => {
      this.cashiers = res;
    });


    this.ApiService.getAvailableStatusPayment().then(res => {

      this.payments_types = res;

    });

    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);

      this.route.paramMap.subscribe(params => {
        if (params.get('id') != null) {
          this.id = params.get('id');

          this.payment.id_policy = this.id;
          this.spinner.show();
          this.ApiService.getPolicyById(this.id).then(res => {
            if (res.status == "success") {
              // this.status = res.data;
              this.payment.id_customer = res.data.id_customer;
            }
            this.spinner.hide();
          });
        }
      });


    }


  }

  get C() { return this.registerPayment.controls; }

  getDataPayment(): void {
    this.payment.id_user = this.user.id;
    this.payment.id_agency = this.user.id_agency;
    this.payment.ins_method = this.registerPayment.get('ins_method').value;
    this.payment.insurance_amount = this.registerPayment.get('insurance_amount').value;
    this.payment.office_amount = this.registerPayment.get('office_amount').value;
    this.payment.adc_method = this.registerPayment.get('adc_method').value;
    this.payment.adc_amount = this.registerPayment.get('adc_amount').value;
    this.payment.id_user_cashier = this.registerPayment.get('cashier').value;
    this.payment.type_transaction = this.registerPayment.get('type_transaction').value;
    this.payment.is_endrose = this.registerPayment.get('is_endrose').value;
    this.payment.concept = this.registerPayment.get('concept').value;
  }



  ValidateCashier(): void {
    let value = this.registerPayment.get('ins_method').value;
    let value2 = this.registerPayment.get('adc_method').value;
    let flag = false;
    if (value == '1' || value2 == '1') {
      flag = true;
    }
    if (!flag) {

      this.registerPayment.get("cashier").setValidators(null);
      this.registerPayment.get("cashier").setValue('0');
      this.registerPayment.get("cashier").disable();
    } else {
      this.registerPayment.get("cashier").setValidators([Validators.required]);
      this.registerPayment.get("cashier").enable();
    }
    this.registerPayment.updateValueAndValidity();
  }

  ValidateConcept(): void {
    let value = this.registerPayment.get('is_endrose').value;

    if (value == '1') {
      this.registerPayment.get("concept").setValidators([Validators.required]);
      this.registerPayment.get("concept").enable();
    } else {
      this.registerPayment.get("concept").setValidators(null);
      this.registerPayment.get("concept").disable();
    }
  }


  private onSubmitPayment(): void {


    this.getDataPayment();
    this.payment.id_dealer = null;
    this.submittedPolicyForm = true;
    if (this.registerPayment.invalid) {
      alert('sdsd');
      return;
    }
    this.savePayment();
  }


  private savePayment(): void {
    this.spinner.show();
    this.ApiService.MakePayment(this.payment).then(res => {
      setTimeout(() => {

        if (res.status == "success") {
          alert(res.message);
          this._location.back();
          // document.getElementById("nav-contact-tab").click();
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      });
    });
  }

}
