import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-vin',
  templateUrl: './add-vin.component.html',
  styleUrls: ['./add-vin.component.scss']
})
export class AddVinComponent implements OnInit {

  localPermissions;
  user;
  registerCard: FormGroup;
  submitted = false;
  editmode = false;
  id_policy;
  id_row;
  card = {
    id_policy: '',
    id_customer: '',
    vin_number: '',

  }

  constructor(public _location: Location, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.registerCard = this.formBuilder.group({

      vin_number: ['', Validators.min(15)],

    }, {
    });


    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
      this.ApiService.getPermissions(this.user.id).then(res => {
        this.localPermissions = res.data;
        this.route.paramMap.subscribe(params => {
          if (params.get('id') != null) {
            this.id_row = params.get('id_item');

            if (this.id_row != 0) {
              this.editmode = true;
              this.setData();

            }
            this.id_policy = params.get('id');
            this.card.id_policy = this.id_policy;
            this.ApiService.getPolicyById(this.id_policy).then(res => {
              if (res.status == "success") {
                // this.status = res.data;
                this.card.id_customer = res.data.id_customer;
              }
              this.spinner.hide();
            });
          }
        });
      });

    }
  }
  get f() { return this.registerCard.controls; }


  getData(): void {
    this.card.vin_number = this.registerCard.get('vin_number').value;
  }
  setData(): void {
    this.spinner.show();
    this.ApiService.getVinId(this.id_row).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.card = res.data;
        this.spinner.hide();
        this.registerCard.get('vin_number').setValue(this.card.vin_number);

      }
      //alert(res.message);
      this.spinner.hide();
    });
  }

  onSubmit(): void {
    this.submitted = true;
    //   this.getDataPayment();
    if (this.registerCard.invalid) {
      return;
    }

    this.getData();
    if (this.editmode) {
      this.Update();
    } else {
      this.Save();
    }
  }

  Save(): void {
    this.spinner.show();
    this.ApiService.saveVin(this.card).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.spinner.hide();
        this._location.back();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }

  Update(): void {
    this.spinner.show();
    this.ApiService.updateVin(this.card, this.id_row).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.spinner.hide();
        this._location.back();
      }
      alert(res.message);
      this.spinner.hide();
    });
  }


}
