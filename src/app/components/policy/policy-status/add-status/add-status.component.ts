import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
import { NgbCalendar, NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-status',
  templateUrl: './add-status.component.html',
  styleUrls: ['./add-status.component.scss']
})
export class AddStatusComponent implements OnInit {
  user;
  id_policy;
  id;
  edtimode = false;
  requeriments;
  registerPayment: FormGroup;
  data = {
    'id_requeriment': '',
    'id_policy': '',
    'status_requerinments': ''
  };
  constructor(private _location: Location, private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.registerPayment = this.formBuilder.group({
      id_requeriment: ['1', Validators.required],
      status: ['Pending', Validators.required],
    }, {

    });



    if (localStorage.getItem("user") != null) {
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
    }


    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id_policy = params.get('id');
      }
      if (params.get('id_item') != null && params.get('id_item') != '0') {
        this.id = params.get('id_item');
        this.edtimode = true;
        // alert("edit mode");
        this.getItem();
       
        this.registerPayment.get("id_requeriment").disable();
      }
      this.spinner.show();
      this.ApiService.getRequeriments().then(res => {
        this.requeriments = res.data;
        this.spinner.hide();
      });

    });







  }
  get C() { return this.registerPayment.controls; }
  getItem(): void {
    this.spinner.show();
    this.ApiService.getStatusById(this.id).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this.data = res.data;
        this.setData();
      }
      this.spinner.hide();
      alert(res.message);
    });


  }

  getData(): void {
    this.data.id_policy = this.id_policy;
    this.data.id_requeriment = this.registerPayment.get('id_requeriment').value;
    this.data.status_requerinments = this.registerPayment.get('status').value;

  }
  setData() {
    this.data.id_policy = this.id_policy;
    this.registerPayment.get('id_requeriment').setValue(this.data.id_requeriment);
    this.registerPayment.get('status').setValue(this.data.status_requerinments);
  }


  onSubmit(): void {
    if (this.edtimode) {
      this.update();
    } else {
      this.save();
    }
  }

  save(): void {
    if (this.registerPayment.invalid) {
      return;
    }
    this.getData();
    this.spinner.show();
    this.ApiService.savePolicyStatus(this.data).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this._location.back();
      }
      this.spinner.hide();
      alert(res.message);
    });
  }

  update(): void {
    if (this.registerPayment.invalid) {
      return;
    }
    this.getData();
    this.spinner.show();
    this.ApiService.updatePolicyStatus(this.data, this.id).then(res => {
      if (res.status == "success") {
        alert(res.message);
        this._location.back();
      }
      this.spinner.hide();
      alert(res.message);
    });
  }
}
