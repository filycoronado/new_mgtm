import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-agency-form',
  templateUrl: './agency-form.component.html',
  styleUrls: ['./agency-form.component.scss']
})
export class AgencyFormComponent implements OnInit {


  id;
  edit = false;
  title="Create Agency";
  registerAgencyForm: FormGroup;
  submitted = false;

  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };

  public agency = {
    name: '',
    code: '',

  };
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {
    this.route.paramMap.subscribe(params => {

      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getAgencyById(this.id).then(res => {
          if (res.status == "success") {
            this.agency = res.data;
            this.setData();
            this.edit = true;
            this.title="Update Agency";
          }

          this.spinner.hide();

        });
      }

    });

    this.registerAgencyForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],

    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerAgencyForm.controls; }



  getData(): void {
    this.agency.name = this.registerAgencyForm.get('name').value;
    this.agency.code = this.registerAgencyForm.get('code').value;

  }

  setData(): void {
    this.registerAgencyForm.get("name").setValue(this.agency.name);
    this.registerAgencyForm.get("code").setValue(this.agency.code);

  }

  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerAgencyForm.invalid) {
      return;
    }

    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }

  }
  private update(): void {
    this.spinner.show();
    this.ApiService.updateAgency(this.agency, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    this.spinner.show();
    this.ApiService.saveAgency(this.agency).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private reset(): void {
    this.agency = {
      name: '',
      code: '',
    };
    this.registerAgencyForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],

    }, {
    });
  }
}
