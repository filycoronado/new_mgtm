import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {
  id;
  public list = [
    { "id": 1, "Name": "Admin", "Description": "View Section Admin", "val": 0 },
    { "id": 2, "Name": "Employee", "Description": "View Employee List", "val": 0 },
    { "id": 3, "Name": "Agencies", "Description": "View Agencies List", "val": 0 },
    { "id": 4, "Name": "Companies", "Description": "View Companies List", "val": 0 },
    { "id": 5, "Name": "Dealers", "Description": "View Dealers List", "val": 0 },
    { "id": 6, "Name": "Status", "Description": "Can Add Status", "val": 0 },
    { "id": 7, "Name": "Edit Status", "Description": "Can Edit Status", "val": 0 },
    { "id": 8, "Name": "Edit Note", "Description": "Can Edit Notes", "val": 0 },
    { "id": 9, "Name": "Edit Payment", "Description": "Can Edit Payments", "val": 0 },
  ];
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getPermissions(this.id).then(res => {
          if (res.status == "success") {
            this.list[0].val = res.data.admin;
            this.list[1].val = res.data.employees;
            this.list[2].val = res.data.agencies;
            this.list[3].val = res.data.companies;
            this.list[4].val = res.data.dealers;
            this.list[5].val = res.data.add_status;
            this.list[6].val = res.data.edit_status;
            this.list[7].val = res.data.edit_note;
            this.list[8].val = res.data.edit_payment;

          }
          this.spinner.hide();
        });
      }
    });

  }

  UpdatePermision(id, val): void {
    this.spinner.show();
    this.ApiService.getUpdatePermission(this.id, id, val).then(res => {
      if (res.status == "success") {
        this.resetTable();
        alert(res.message);
      }
      this.spinner.hide();
    });
  }

  private resetTable(): void {
    this.ApiService.getPermissions(this.id).then(res => {
      if (res.status == "success") {
        this.list[0].val = res.data.admin;
        this.list[1].val = res.data.employees;
        this.list[2].val = res.data.agencies;
        this.list[3].val = res.data.companies;
        this.list[4].val = res.data.dealers;
        this.list[5].val = res.data.add_status;
        this.list[6].val = res.data.edit_status;
        this.list[7].val = res.data.edit_note;
        this.list[8].val = res.data.edit_payment;


      }

    });
  }

}
