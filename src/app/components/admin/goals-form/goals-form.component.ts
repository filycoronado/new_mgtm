import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/service';

@Component({
  selector: 'app-goals-form',
  templateUrl: './goals-form.component.html',
  styleUrls: ['./goals-form.component.scss']
})
export class GoalsFormComponent implements OnInit {


  id;
  id_goal;
  edit = false;
  title = "Create Goal";
  agencies;
  registerEmployeeForm: FormGroup;
  submitted = false;
  model: NgbDateStruct;
  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };

  public goal = {
    name: '',
    start_date: '',
    end_date: '',
    id_user: '',
    nb: '',
    nb_adc: '',
  };
  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      if (params.get('id_user') != null) {
        this.id = params.get('id_user');//id user is here
        this.goal.id_user = this.id;
      }

      if (params.get("id_goal") != null) {//id user =
        this.id_goal = params.get('id_goal');

        this.spinner.show();
        this.ApiService.getGoalById(this.id_goal).then(res => {
          if (res.status == "success") {
            this.goal = res.data;
            this.setData();
            this.edit = true;
            this.title = "Update Goal";
          }
          this.spinner.hide();
        });
      }
    });


    this.registerEmployeeForm = this.formBuilder.group({
      start_date: [this.calendar.getToday(), Validators.required],
      end_date: [this.calendar.getToday(), Validators.required],
      nb: [0, Validators.required],
      nb_adc: [0, Validators.required],
      name: ['', Validators.required],

    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerEmployeeForm.controls; }



  getData(): void {
    this.goal.name = this.registerEmployeeForm.get('name').value;
    this.goal.start_date = this.registerEmployeeForm.get('start_date').value;
    this.goal.end_date = this.registerEmployeeForm.get('end_date').value;
    this.goal.nb = this.registerEmployeeForm.get('nb').value;
    this.goal.nb_adc = this.registerEmployeeForm.get('nb_adc').value;

  }
  setData(): void {
    let arr = this.goal.start_date.split("-");
    this.registerEmployeeForm.get('start_date').setValue({
      year: parseInt(arr[0]),
      month: parseInt(arr[1]),
      day: parseInt(arr[2])
    });

    let arr2 = this.goal.end_date.split("-");
    this.registerEmployeeForm.get('end_date').setValue({
      year: parseInt(arr2[0]),
      month: parseInt(arr2[1]),
      day: parseInt(arr2[2])
    });
    this.registerEmployeeForm.get('name').setValue(this.goal.name);
    this.registerEmployeeForm.get('nb').setValue(this.goal.nb);
    this.registerEmployeeForm.get('nb_adc').setValue(this.goal.nb_adc);


  }
  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerEmployeeForm.invalid) {
      return;
    }


    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }


  }
  private update(): void {

    // display form values on success
    this.spinner.show();
    this.ApiService.updateGoal(this.goal, this.id_goal).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          //  this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    // display form values on success
    this.spinner.show();
    this.ApiService.saveGoals(this.goal).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }

  private reset(): void {
    this.goal = {
      name: '',
      start_date: '',
      end_date: '',
      id_user: this.id,
      nb: '',
      nb_adc: '',
    };

    this.registerEmployeeForm = this.formBuilder.group({
      start_date: [this.calendar.getToday(), Validators.required],
      end_date: [this.calendar.getToday(), Validators.required],
      name: ['', Validators.required],
      nb: [0, Validators.required],
      nb_adc: [0, Validators.required],
    }, {

    });

  }

}
