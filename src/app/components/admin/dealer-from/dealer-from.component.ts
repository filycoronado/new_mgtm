import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/service';
@Component({
  selector: 'app-dealer-from',
  templateUrl: './dealer-from.component.html',
  styleUrls: ['./dealer-from.component.scss']
})
export class DealerFromComponent implements OnInit {

  id;
  edit = false;
  title = "Create Dealer";
  registerDealerForm: FormGroup;
  submitted = false;

  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };

  public dealer = {
    name: '',
    location: '',
    zone: '',

  };
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getDealerById(this.id).then(res => {
          if (res.status == "success") {
            this.dealer = res.data;
            this.setData();
            this.edit = true;
            this.title = "Update Dealer";
          }
          this.spinner.hide();
        });
      }

    });

    this.registerDealerForm = this.formBuilder.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      zone: ['NORTH', Validators.required],

    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerDealerForm.controls; }



  getData(): void {
    this.dealer.name = this.registerDealerForm.get('name').value;
    this.dealer.location = this.registerDealerForm.get('location').value;
    this.dealer.zone = this.registerDealerForm.get('zone').value;

  }

  setData(): void {
    this.registerDealerForm.get("name").setValue(this.dealer.name);
    this.registerDealerForm.get("location").setValue(this.dealer.location);
    this.registerDealerForm.get("zone").setValue(this.dealer.zone);

  }

  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerDealerForm.invalid) {
      return;
    }

    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }

  }
  private update(): void {
    this.spinner.show();
    this.ApiService.updateDealer(this.dealer, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    this.spinner.show();
    this.ApiService.saveDealer(this.dealer).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private reset(): void {
    this.dealer = {
      name: '',
      location: '',
      zone: '',
    };
    this.registerDealerForm = this.formBuilder.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      zone: ['NORTH', Validators.required],

    }, {

    });
  }
}
