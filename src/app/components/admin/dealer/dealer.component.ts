import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.scss']
})
export class DealerComponent implements OnInit {
  public list;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.ApiService.getAvailableDealer().then(res => {
      if (res.status == "success") {
        this.list = res.dealers;
        this.spinner.hide();
      } else {

        this.spinner.hide();
      }


    });
  }

}
