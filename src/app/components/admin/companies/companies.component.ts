import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  public list;
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.ApiService.getAvailableCompanies().then(res => {
      if (res.status == "success") {
        this.list = res.companies;
        this.spinner.hide();
      } else {

        this.spinner.hide();
      }
      

    });
  }
}
