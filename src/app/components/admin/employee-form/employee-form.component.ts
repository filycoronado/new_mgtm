import { Component, OnInit } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {
  id;
  edit = false;
  title = "Create Employee";
  agencies;
  registerEmployeeForm: FormGroup;
  submitted = false;
  model: NgbDateStruct;
  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };

  public employee = {
    create_date: '',
    username: '',
    gender: '',
    full_name: '',
    dob: '',
    role: '',
    id_agency: '',
    is_cashier: '',
  };
  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getEmployeeById(this.id).then(res => {

          if (res.status == "success") {
            this.employee = res.data;
            if (this.employee.role == "ADMIN") {
              this.employee.role = '1';
            } else {
              this.employee.role = '2';
            }



            this.setData();
            this.edit = true;
            this.title = "Update Employee";
          }
          this.spinner.hide();
        });
      }
    });


    this.spinner.show();
    this.ApiService.getAgencies().then(res => {

      this.agencies = res;
      this.spinner.hide();

    });

    this.registerEmployeeForm = this.formBuilder.group({
      dob: [this.calendar.getToday(), Validators.required],
      username: ['', Validators.required],
      full_name: ['', Validators.required],
      gender: ['1', Validators.required],
      role: ['1', Validators.required],
      id_agency: ['1', Validators.required],
      cashier: ['0', Validators.required],

    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerEmployeeForm.controls; }



  getData(): void {
    this.employee.dob = this.registerEmployeeForm.get('dob').value;
    this.employee.username = this.registerEmployeeForm.get('username').value;
    this.employee.gender = this.registerEmployeeForm.get('gender').value;
    this.employee.role = this.registerEmployeeForm.get('role').value;
    this.employee.id_agency = this.registerEmployeeForm.get('id_agency').value;
    this.employee.full_name = this.registerEmployeeForm.get('full_name').value;
    this.employee.is_cashier = this.registerEmployeeForm.get('cashier').value;
  }
  setData(): void {
    let arr = this.employee.dob.split("-");
    this.registerEmployeeForm.get('dob').setValue({
      year: parseInt(arr[0]),
      month: parseInt(arr[1]),
      day: parseInt(arr[2])
    });
    this.registerEmployeeForm.get('username').setValue(this.employee.username);
    this.registerEmployeeForm.get('gender').setValue(this.employee.gender);
    this.registerEmployeeForm.get('role').setValue(this.employee.role);
    this.registerEmployeeForm.get('id_agency').setValue(this.employee.id_agency);
    this.registerEmployeeForm.get('full_name').setValue(this.employee.full_name);
    this.registerEmployeeForm.get('cashier').setValue(this.employee.is_cashier);

  }
  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerEmployeeForm.invalid) {
      return;
    }


    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }


  }
  private update(): void {

    // display form values on success
    this.spinner.show();
    this.ApiService.updateEmployee(this.employee, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          //  this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    // display form values on success
    this.spinner.show();
    this.ApiService.saveEmployee(this.employee).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }

  private reset(): void {
    this.employee = {
      create_date: '',
      username: '',
      gender: '',
      full_name: '',
      dob: '',
      role: '',
      id_agency: '',
      is_cashier: '',
    };

    this.registerEmployeeForm = this.formBuilder.group({
      dob: [this.calendar.getToday(), Validators.required],
      username: ['', Validators.required],
      full_name: ['', Validators.required],
      gender: ['1', Validators.required],
      role: ['1', Validators.required],
      id_agency: ['1', Validators.required],
      cashier: ['0', Validators.required],

    }, {

    });
  }
}
