import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Location } from '@angular/common';
@Component({
  selector: 'app-notes-form',
  templateUrl: './notes-form.component.html',
  styleUrls: ['./notes-form.component.scss']
})
export class NotesFormComponent implements OnInit {


  id;
  id_note;
  edit = false;
  title = "Create Note";
  registerAgencyForm: FormGroup;
  submitted = false;



  public note = {
    note: '',
    id_user: '',

  };

  public Editor = ClassicEditor;


  constructor(private _location: Location, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {
    this.route.paramMap.subscribe(params => {

      if (params.get('id') != null) {
        this.id = params.get('id');
        this.note.id_user = this.id;
      }

      if (params.get('id_note') != null) {
        this.id_note = params.get('id_note');
        this.edit = true;
        this.title = "Update Note";

        this.spinner.show();
        this.ApiService.get_noteById(this.id_note).then(res => {
          setTimeout(() => {
            if (res.status == "success") {
              this.note = res.data;
              this.setData();
              alert(res.message);

            } else {
              alert(res.message);
            }
            this.spinner.hide();
          }, 2000);
        });
      }

    });

    this.registerAgencyForm = this.formBuilder.group({
      note: ['Hello', Validators.required],


    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerAgencyForm.controls; }



  getData(): void {
    this.note.note = this.registerAgencyForm.get('note').value;

  }

  setData(): void {
    this.registerAgencyForm.get("note").setValue(this.note.note);


  }

  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerAgencyForm.invalid) {
      return;
    }

    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }

  }
  private update(): void {
    this.spinner.show();
    this.ApiService.updateNote(this.note, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    this.spinner.show();
    this.ApiService.save_note(this.note).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
          this._location.back();
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private reset(): void {
    this.note = {
      note: '',
      id_user: this.id,
    };
    this.registerAgencyForm = this.formBuilder.group({
      note: ['', Validators.required],


    }, {
    });
  }
}
