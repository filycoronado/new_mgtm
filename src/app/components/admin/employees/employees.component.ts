import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
  public listEmployees;
  dtOptions: DataTables.Settings = {};
  dtTrigger: any = new Subject();
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();

    this.dtOptions = {
   
        pagingType: 'full_numbers',
        pageLength: 10,
   
    };
    this.ApiService.getAvailableUsers().then(res => {
      if (res.status == "success") {
        this.listEmployees = res.employees;
        this.spinner.hide();
        this.dtTrigger.next();
        this.extractData(res.employees);
      } else {
        alert(res.message);
        this.spinner.hide();
      }

    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(list) {

    return list || {};
  }
}
