import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/service/service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.scss']
})
export class CompanyFormComponent implements OnInit {

  id;
  edit = false;
  title = "Create Company";
  agencies;
  registerCompanyForm: FormGroup;
  submitted = false;
  model: NgbDateStruct;
  date: { year: number, month: number };
  minDate = { year: 1950, month: 1, day: 1 };

  public company = {
    name: '',
    site_url: '',
    phone: '',
    id_llc: '1',
  };
  constructor(private calendar: NgbCalendar, public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private route: ActivatedRoute) {

  }


  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.id = params.get('id');
        this.spinner.show();
        this.ApiService.getCompanyById(this.id).then(res => {

          if (res.status == "success") {
            this.company = res.data;
            this.setData();
            this.edit = true;
            this.title = "Update Company";
          }
          this.spinner.hide();
        });
      }
    });




    this.registerCompanyForm = this.formBuilder.group({
      name: ['', Validators.required],
      website: ['', Validators.required],
      phone: ['', Validators.required],
      id_llc: ['1', Validators.required],


    }, {

    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerCompanyForm.controls; }



  getData(): void {
    this.company.name = this.registerCompanyForm.get('name').value;
    this.company.site_url = this.registerCompanyForm.get('website').value;
    this.company.phone = this.registerCompanyForm.get('phone').value;
    this.company.id_llc = this.registerCompanyForm.get('id_llc').value;
  }
  setData(): void {
    this.registerCompanyForm.get('name').setValue(this.company.name);
    this.registerCompanyForm.get('website').setValue(this.company.site_url);
    this.registerCompanyForm.get('phone').setValue(this.company.phone);
    this.registerCompanyForm.get('id_llc').setValue(this.company.id_llc);
  }
  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerCompanyForm.invalid) {
      return;
    }


    // display form values on success
    if (!this.edit) {
      this.save();
    } else {
      this.update();
    }


  }
  private update(): void {

    // display form values on success
    this.spinner.show();
    this.ApiService.updateCompany(this.company, this.id).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          //  this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }
  private save(): void {
    // display form values on success
    this.spinner.show();
    this.ApiService.saveCompany(this.company).then(res => {
      setTimeout(() => {
        if (res.status == "success") {
          this.reset();
          alert(res.message);
        } else {
          alert(res.message);
        }
        this.spinner.hide();
      }, 2000);
    });
  }

  private reset(): void {
    this.company = {
      name: '',
      site_url: '',
      phone: '',
      id_llc: '1',
    };

    this.registerCompanyForm = this.formBuilder.group({
      name: ['', Validators.required],
      website: ['', Validators.required],
      phone: ['', Validators.required],
      id_llc: ['1', Validators.required],
    }, {

    });
  }

}
