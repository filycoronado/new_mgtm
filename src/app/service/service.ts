import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class ApiService {

    public user: any;

    constructor(
        public globalService: GlobalService,
        private http: HttpClient
    ) { }

    get() {
        if (localStorage.getItem('user')) {
            this.user = JSON.parse(localStorage.getItem('user'));
            return this.user;
        }
        return false;
    }

    set(user) {
        localStorage.setItem('user', JSON.stringify(user));
    }

    logout(): Promise<any> {
        this.user = '';
        localStorage.removeItem('user');
        return Promise.resolve(true);
    }

    register(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/register', { user: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    login(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/login', { user: data }).subscribe((res) => {
                //console.log(res);
                this.user = res.data;
                //console.log(this.user);
                if (data.remember) {
                    localStorage.setItem('user', JSON.stringify(this.user));
                }
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    updatePassword(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/update_password', { user: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }
    sendEmailRecovery(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/mail_recovery', { user: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }
    saveCart(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/save_cart', { cart: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }

    getProduct(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'user/get_product&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }
    getCarUser(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'user/get_cart&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    deleteCartItem(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'user/delete_car_item&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise 
    }


    save_sell(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/save_sell', { products: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }
    confimrEmail(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'user/confirm_mail&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise 
    }
    getUserById(id) {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'user/get_user_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise 
    }
    updateUser(data) {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/update_user', { user: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise 
    }



    saveQuote(datafront, databack, email, color): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/send_quote', { front: datafront, back: databack, email: email, color: color }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }


    SendMessage(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/send_message', { form: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise 
    }

    AddTapa(idProduct, id_user): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'user/add_tapa', { idProduct, id_user }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }



    /**new methods */
    test(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'api/test', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    doLogin(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'employee/login', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }

    getAvailableUsers(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'employee/get_available_users', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    saveEmployee(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'employee/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }


    updateEmployee(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'employee/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }
    getEmployeeById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'employee/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getAvailableCashiers(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'employee/get_cashiers', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    getAgencyById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'agency/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    getAgencies(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'agency/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    saveAgency(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'agency/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updateAgency(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'agency/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**company */

    getAvailableCompanies(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'company/get_companies', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getAvailableCompaniesByLLC(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'company/get_by_llc&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    saveCompany(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'company/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    updateCompany(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'company/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getCompanyById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'company/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**Delers */
    saveDealer(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'dealer/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    updateDealer(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'dealer/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getDealerById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'dealer/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getAvailableDealer(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'dealer/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**Permisions */

    getPermissions(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'permissions/get_permission_user&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getUpdatePermission(id_user, id_permision, value): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'permissions/update', { id_user: id_user, id_permision: id_permision, value: value }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    //**Notes */Get_notes_user
    Get_notes_user(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_notes_user&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    save_note(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'notes/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    save_note_policy(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'notes/save_note', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    update_note_policy(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'notes/update_note', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    getNoteById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_note_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    get_noteById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_notes_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updateNote(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'notes/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    getNotesByPolicy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_notes_by_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    Get_types(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_types', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    Get_notes_by_policy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/get_notes_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }




    change_statusNote(id, status): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'notes/change_notes_policy&id=' + id + '&status=' + status, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    /**Goals */
    getGoalsByUser(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'goals/get_goals&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    saveGoals(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'goals/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }

    getGoalById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'goals/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updateGoal(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'goals/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise   
    }

    /**Customer */
    saveCustomer(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'customer/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    getCustomerById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'customer/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updateCustomer(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'customer/update', { id: id, data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**Save Policy */
    savePolicy(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'policy/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    updatePolicy(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'policy/update', { id: id, data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getPolicyById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'policy/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    /**llcType */
    getLLCTypes(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'llctype/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**status Policy */
    getAvailableStatusPolicy(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'statuspolicy/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    getAvailableSubStatusPolicy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'statuspolicy/get_available_sub_status&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    /**Policy Type */
    getAvailableStatusPolicyTypes(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'policytypes/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**Status payment */

    getAvailableStatusPayment(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'payment_status/get_available', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**Save Payment */
    savePayment(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'ticket/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


  
    MakePayment(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'ticket/make_payment', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updatePayment(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'ticket/update', { id: id, data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getPayments(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'ticket/get_paymet_by_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getPaymentById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'ticket/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }
    /**search Customer */
    getCustomers(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'customer/serach', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**get Files Types */

    getTypesFiles(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'types_files/get_type_files', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    getFilesByPolicy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'policy/get_files_by_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    getPhotosByPolicy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'policy/get_photos_by_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    /**get status  */
    getRequermientsPolicy(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'requirements_policy/get_available', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    getRequeriments(): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'requirements_policy/get_requeriments', {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    savePolicyStatus(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'requirements_policy/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    updatePolicyStatus(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'requirements_policy/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }



    getStatusById(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'requirements_policy/get_by_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    /**Card Policy */
    saveCard(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'bank/save', { data: data }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


    updateCard(data, id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.globalService.serverUrl + 'bank/update', { data: data, id: id }).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    Get_bank_by_policy(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'bank/get_bank_policy&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }

    getCard(id): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.globalService.serverUrl + 'bank/get_card_id&id=' + id, {}).subscribe((res) => {
                //console.log(res);
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise  
    }


        /**Vins Policy */
        saveVin(data): Promise<any> {
            return new Promise((resolve, reject) => {
                return this.http.post<any>(this.globalService.serverUrl + 'vin/save', { data: data }).subscribe((res) => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
            });//promise  
        }
    
    
        updateVin(data, id): Promise<any> {
            return new Promise((resolve, reject) => {
                return this.http.post<any>(this.globalService.serverUrl + 'vin/update', { data: data, id: id }).subscribe((res) => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
            });//promise  
        }

        Get_vin_by_policy(id): Promise<any> {
            return new Promise((resolve, reject) => {
                return this.http.get<any>(this.globalService.serverUrl + 'vin/get_all&id=' + id, {}).subscribe((res) => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
            });//promise  
        }

        getVinId(id): Promise<any> {
            return new Promise((resolve, reject) => {
                return this.http.get<any>(this.globalService.serverUrl + 'vin/get_vin_id&id=' + id, {}).subscribe((res) => {
                    //console.log(res);
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
            });//promise  
        }

}
