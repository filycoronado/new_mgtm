import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TablesComponent } from './tables/tables.component';
import { IconsComponent } from './icons/icons.component';
import { TypographyComponent } from './typography/typography.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AccordionsComponent } from './accordions/accordions.component';
import { BadgesComponent } from './badges/badges.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CarouselComponent } from './carousel/carousel.component';
import { TabsComponent } from './tabs/tabs.component';
import { LoginComponentComponent } from './login/login-component/login-component.component';
import { EmployeesComponent } from './components/admin/employees/employees.component';
import { EmployeeFormComponent } from './components/admin/employee-form/employee-form.component';
import { AgenciesComponent } from './components/admin/agencies/agencies.component';
import { AgencyFormComponent } from './components/admin/agency-form/agency-form.component';
import { CompaniesComponent } from './components/admin/companies/companies.component';
import { CompanyFormComponent } from './components/admin/company-form/company-form.component';
import { DealerComponent } from './components/admin/dealer/dealer.component';
import { DealerFromComponent } from './components/admin/dealer-from/dealer-from.component';
import { PermissionsComponent } from './components/admin/permissions/permissions.component';
import { NotesComponent } from './components/admin/notes/notes.component';
import { NotesFormComponent } from './components/admin/notes-form/notes-form.component';
import { GoalsComponent } from './components/admin/goals/goals.component';
import { GoalsFormComponent } from './components/admin/goals-form/goals-form.component';
import { CustomerFormComponent } from './components/customer/customer-form/customer-form.component';
import { CustomerSearchComponent } from './components/customer/customer-form/customer-search/customer-search.component';
import { CustomerEditComponent } from './components/customer/customer-form/customer-edit/customer-edit.component';
import { CustomerPolicyEditComponent } from './components/customer/customer-form/customer-policy-edit/customer-policy-edit.component';
import { CustomerPaymentsComponent } from './components/customer/customer-form/customer-payments/customer-payments.component';
import { EditTicektComponent } from './components/customer/customer-form/edit-ticekt/edit-ticekt.component';
import { CustomerFilesComponent } from './components/customer/customer-form/customer-files/customer-files.component';
import { UploadFilesComponent } from './components/customer/customer-form/upload-files/upload-files.component';
import { PolicyPhotosComponent } from './components/customer/customer-form/policy-photos/policy-photos.component';
import { PolicyStatusComponent } from './components/policy/policy-status/policy-status.component';
import { AddStatusComponent } from './components/policy/policy-status/add-status/add-status.component';
import { CustomerNotesComponent } from './components/customer/customer-form/customer-notes/customer-notes.component';
import { AddNoteComponent } from './components/customer/customer-form/customer-notes/add-note/add-note.component';
import { BankComponent } from './components/customer/customer-form/bank/bank.component';
import { AddCardComponent } from './components/customer/customer-form/add-card/add-card.component';
import { PaymentPolicyComponent } from './components/policy/policy-status/payment-policy/payment-policy.component';
import { VinsComponent } from './components/policy/policy-status/vins/vins.component';
import { AddVinComponent } from './components/policy/policy-status/add-vin/add-vin.component';
import { SalesReportComponent } from './components/reports/sales-report/sales-report.component';




const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'forms', component: FormsComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'tables', component: TablesComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'typography', component: TypographyComponent },
  { path: 'alerts', component: AlertsComponent },
  { path: 'accordions', component: AccordionsComponent },
  { path: 'badges', component: BadgesComponent },
  { path: 'progressbar', component: ProgressbarComponent },
  { path: 'breadcrumbs', component: BreadcrumbsComponent },
  { path: 'pagination', component: PaginationComponent },
  { path: 'dropdowns', component: DropdownComponent },
  { path: 'tooltips', component: TooltipsComponent },
  { path: 'carousel', component: CarouselComponent },
  { path: 'tabs', component: TabsComponent },
  { path: 'login', component: LoginComponentComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'employees/form', component: EmployeeFormComponent },
  { path: 'employees/form/:id', component: EmployeeFormComponent },
  { path: 'employees/permission/:id', component: PermissionsComponent },
  { path: 'employees/notes/:id', component: NotesComponent },
  { path: 'employees/notes/form/:id', component: NotesFormComponent },
  { path: 'employees/notes/form/:id/:id_note', component: NotesFormComponent },
  { path: 'employees/notes/goals/:id', component: GoalsComponent },
  { path: 'employee/goal/form/:id_user/:id_goal', component: GoalsFormComponent },
  { path: 'employee/goal/form/:id_user', component: GoalsFormComponent },
  { path: 'agencies', component: AgenciesComponent },
  { path: 'agency/form', component: AgencyFormComponent },
  { path: 'agency/form/:id', component: AgencyFormComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'company/form', component: CompanyFormComponent },
  { path: 'company/form/:id', component: CompanyFormComponent },
  { path: 'dealers', component: DealerComponent },
  { path: 'dealer/form', component: DealerFromComponent },
  { path: 'dealer/form/:id', component: DealerFromComponent },
  { path: 'new/customer', component: CustomerFormComponent },
  { path: 'search/customer', component: CustomerSearchComponent },
  { path: 'customer/edit/:id', component: CustomerEditComponent },
  { path: 'policy/edit/:id', component: CustomerPolicyEditComponent },
  { path: 'policy/payments/:id', component: CustomerPaymentsComponent },
  { path: 'edit/ticekt/:id', component: EditTicektComponent },
  { path: 'policy/files/:id', component: CustomerFilesComponent },
  { path: 'policy/new/files/:id/:type', component: UploadFilesComponent },
  { path: 'policy/photos/:id', component: PolicyPhotosComponent },
  { path: 'policy/status/:id', component: PolicyStatusComponent },
  { path: 'policy/new/status/:id/:id_item', component: AddStatusComponent },
  { path: 'policy/notes/:id', component: CustomerNotesComponent },
  { path: 'policy/new/note/:id/:id_item', component: AddNoteComponent },
  { path: 'policy/bank/:id', component: BankComponent },
  { path: 'policy/new/card/:id/:id_item', component: AddCardComponent },
  { path: 'policy/new/payment/:id', component: PaymentPolicyComponent },
  { path: 'policy/vins/:id', component: VinsComponent },
  { path: 'policy/new/vin/:id/:id_item', component: AddVinComponent },
  { path: 'report/sales', component: SalesReportComponent },







];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
