import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/service/service';
import { GlobalService } from '../service/global.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  //@Input() Permissions:[];
  public samplePagesCollapsed = true;



  public user;
  permisions = [];
  vartest = false;

  constructor(public ApiService: ApiService ,public globalservice: GlobalService) {

  }
  ngOnInit() {
    if (localStorage.getItem("user") != null) {
      this.vartest = true;
      this.user = localStorage.getItem("user");
      this.user = JSON.parse(this.user);
  
      this.getPermisions();
    } else {
      this.vartest = false;
    }
  }


  getPermisions(): void {
    this.ApiService.getPermissions(this.user.id).then(res => {
      if (res.status == "success") {
        this.globalservice.permisions= res.data;
        this.permisions=this.globalservice.permisions;
        //console.log(this.permisions);
      }

    });
  }
}
