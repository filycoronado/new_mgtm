import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';



import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TablesComponent } from './tables/tables.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AccordionsComponent } from './accordions/accordions.component';
import { BadgesComponent } from './badges/badges.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CarouselComponent } from './carousel/carousel.component';
import { TabsComponent } from './tabs/tabs.component';
import { LoginComponentComponent } from './login/login-component/login-component.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { EmployeesComponent } from './components/admin/employees/employees.component';
import { EmployeeFormComponent } from './components/admin/employee-form/employee-form.component';
import { AgenciesComponent } from './components/admin/agencies/agencies.component';
import { AgencyFormComponent } from './components/admin/agency-form/agency-form.component';
import { CompaniesComponent } from './components/admin/companies/companies.component';
import { CompanyFormComponent } from './components/admin/company-form/company-form.component';
import { DealerComponent } from './components/admin/dealer/dealer.component';
import { DealerFromComponent } from './components/admin/dealer-from/dealer-from.component';
import { PermissionsComponent } from './components/admin/permissions/permissions.component';
import { NotesComponent } from './components/admin/notes/notes.component';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NotesFormComponent } from './components/admin/notes-form/notes-form.component';
import { GoalsComponent } from './components/admin/goals/goals.component';
import { GoalsFormComponent } from './components/admin/goals-form/goals-form.component';
import { CustomerFormComponent } from './components/customer/customer-form/customer-form.component';

import { NgxMaskModule } from 'ngx-mask'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomerSearchComponent } from './components/customer/customer-form/customer-search/customer-search.component';
import { CustomerEditComponent } from './components/customer/customer-form/customer-edit/customer-edit.component';
import { CustomerPolicyEditComponent } from './components/customer/customer-form/customer-policy-edit/customer-policy-edit.component';
import { CustomerPaymentsComponent } from './components/customer/customer-form/customer-payments/customer-payments.component';
import { EditTicektComponent } from './components/customer/customer-form/edit-ticekt/edit-ticekt.component';
import { CustomerFilesComponent } from './components/customer/customer-form/customer-files/customer-files.component';
import { UploadFilesComponent } from './components/customer/customer-form/upload-files/upload-files.component';
import { PolicyPhotosComponent } from './components/customer/customer-form/policy-photos/policy-photos.component';
import { BarOptionsComponent } from './components/utils/bar-options/bar-options.component';
import { PolicyStatusComponent } from './components/policy/policy-status/policy-status.component';
import { AddStatusComponent } from './components/policy/policy-status/add-status/add-status.component';
import { CustomerNotesComponent } from './components/customer/customer-form/customer-notes/customer-notes.component';
import { AddNoteComponent } from './components/customer/customer-form/customer-notes/add-note/add-note.component';
import { BankComponent } from './components/customer/customer-form/bank/bank.component';
import { AddCardComponent } from './components/customer/customer-form/add-card/add-card.component';
import { PaymentPolicyComponent } from './components/policy/policy-status/payment-policy/payment-policy.component';
import { VinsComponent } from './components/policy/policy-status/vins/vins.component';
import { AddVinComponent } from './components/policy/policy-status/add-vin/add-vin.component';
import { DataTablesModule } from 'angular-datatables';
import { SalesReportComponent } from './components/reports/sales-report/sales-report.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    FormsComponent,
    ButtonsComponent,
    TablesComponent,
    TypographyComponent,
    IconsComponent,
    AlertsComponent,
    AccordionsComponent,
    BadgesComponent,
    ProgressbarComponent,
    BreadcrumbsComponent,
    PaginationComponent,
    DropdownComponent,
    TooltipsComponent,
    CarouselComponent,
    TabsComponent,
    LoginComponentComponent,
    EmployeesComponent,
    EmployeeFormComponent,
    AgenciesComponent,
    AgencyFormComponent,
    CompaniesComponent,
    CompanyFormComponent,
    DealerComponent,
    DealerFromComponent,
    PermissionsComponent,
    NotesComponent,
    NotesFormComponent,
    GoalsComponent,
    GoalsFormComponent,
    CustomerFormComponent,
    CustomerSearchComponent,
    CustomerEditComponent,
    CustomerPolicyEditComponent,
    CustomerPaymentsComponent,
    EditTicektComponent,
    CustomerFilesComponent,
    UploadFilesComponent,
    PolicyPhotosComponent,
    BarOptionsComponent,
    PolicyStatusComponent,
    AddStatusComponent,
    CustomerNotesComponent,
    AddNoteComponent,
    BankComponent,
    AddCardComponent,
    PaymentPolicyComponent,
    VinsComponent,
    AddVinComponent,
    SalesReportComponent,


  ],
  imports: [

    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    NgxSpinnerModule,
    CKEditorModule,
    NgxMaskModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    DataTablesModule


  ],
  exports: [NgxSpinnerModule],
  providers: [

  ],
  bootstrap: [AppComponent]
})

export class AppModule { }