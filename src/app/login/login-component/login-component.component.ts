import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  login = {
    username: "",
    password: "",
    id_agency: "101"
  }
  constructor(public ApiService: ApiService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      code: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],

    }, {

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }



  getData(): void {
    this.login.id_agency = this.loginForm.get('code').value;
    this.login.username = this.loginForm.get('username').value;
    this.login.password = this.loginForm.get('password').value;
  }
  onSubmit(): void {
    this.getData();
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // display form values on success
    this.spinner.show();
    this.ApiService.doLogin(this.login).then(res => {
      console.log(res.user);
      setTimeout(() => {
        localStorage.setItem("user", JSON.stringify(res.user));
        location.reload();
      }, 5000);
    });
  }

}
