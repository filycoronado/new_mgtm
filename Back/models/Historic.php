<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "historic".
 *
 * @property int $id
 * @property int $id_policy
 * @property int $id_status
 * @property int $id_sub_status
 * @property int $create_date
 * @property int $enabled
 *
 * @property Policy $policy
 * @property Statuspolicy $status
 * @property PolicySubStatus $subStatus
 */
class Historic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'historic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_policy', 'id_status', 'id_sub_status', 'create_date'], 'required'],
            [['id_policy', 'id_status', 'id_sub_status', 'create_date', 'enabled'], 'integer'],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Statuspolicy::className(), 'targetAttribute' => ['id_status' => 'id']],
            [['id_sub_status'], 'exist', 'skipOnError' => true, 'targetClass' => PolicySubStatus::className(), 'targetAttribute' => ['id_sub_status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_policy' => 'Id Policy',
            'id_status' => 'Id Status',
            'id_sub_status' => 'Id Sub Status',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuspolicy::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubStatus()
    {
        return $this->hasOne(PolicySubStatus::className(), ['id' => 'id_sub_status']);
    }
}
