<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_policy
 * @property int $id_customer
 * @property int $id_user_asigned
 * @property int $id_note_type
 * @property string $follow_date
 * @property string $create_date
 * @property string $note
 * @property int $status_note
 * @property int $enabled
 *
 * @property Customer $customer
 * @property NotesType $noteType
 * @property Policy $policy
 * @property User $userAsigned
 * @property User $user
 */
class Notes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_policy', 'id_customer', 'id_user_asigned', 'id_note_type', 'follow_date', 'create_date', 'note'], 'required'],
            [['id_user', 'id_policy', 'id_customer', 'id_user_asigned', 'id_note_type', 'status_note', 'enabled'], 'integer'],
            [['follow_date', 'create_date'], 'safe'],
            [['note'], 'string'],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
            [['id_note_type'], 'exist', 'skipOnError' => true, 'targetClass' => NotesType::className(), 'targetAttribute' => ['id_note_type' => 'id']],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_user_asigned'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_asigned' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_policy' => 'Id Policy',
            'id_customer' => 'Id Customer',
            'id_user_asigned' => 'Id User Asigned',
            'id_note_type' => 'Id Note Type',
            'follow_date' => 'Follow Date',
            'create_date' => 'Create Date',
            'note' => 'Note',
            'status_note' => 'Status Note',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoteType()
    {
        return $this->hasOne(NotesType::className(), ['id' => 'id_note_type'])->select("id,name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy'])->select("id,policy_number");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAsigned()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_asigned'])->select("id,full_name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->select("id,full_name");
    }
}
