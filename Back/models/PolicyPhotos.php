<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "policy_photos".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_policy
 * @property int|null $id_file_type
 * @property string $file
 * @property string $create_date
 * @property int $enabled
 *
 * @property User $user
 * @property Policy $policy
 * @property TypesFile $fileType
 */
class PolicyPhotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'policy_photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_policy', 'file', 'create_date'], 'required'],
            [['id_user', 'id_policy', 'id_file_type', 'enabled'], 'integer'],
            [['file'], 'string'],
            [['create_date'], 'safe'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_file_type'], 'exist', 'skipOnError' => true, 'targetClass' => TypesFile::className(), 'targetAttribute' => ['id_file_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_policy' => 'Id Policy',
            'id_file_type' => 'Id File Type',
            'file' => 'File',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileType()
    {
        return $this->hasOne(TypesFile::className(), ['id' => 'id_file_type']);
    }
}
