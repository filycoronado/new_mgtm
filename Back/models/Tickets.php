<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property int $id 
 * @property int $id_agency
 * @property int $id_policy
 * @property int $id_customer
 * @property int $id_user
 * @property int|null $id_user_cashier
 * @property int|null $id_dealer
 * @property int $type_transaction
 * @property string $create_date
 * @property int $ins_method
 * @property int $insurance_amount
 * @property int $office_amount
 * @property int $adc_method
 * @property int $adc_amount
 * @property int $is_credit
 * @property int $is_endorse
 * @property int $concept
 * @property int $enabled
 *
 * @property User $userCashier
 * @property Customer $customer
 * @property Dealer $dealer
 * @property PaymentStatus $typeTransaction
 * @property Policy $policy
 * @property User $user
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_agency', 'id_policy', 'id_customer', 'id_user', 'type_transaction', 'create_date', 'ins_method', 'insurance_amount', 'office_amount', 'adc_method', 'adc_amount'], 'required'],
            [['id_agency', 'id_policy', 'id_customer', 'id_user', 'id_user_cashier', 'id_dealer', 'type_transaction', 'ins_method', 'adc_amount', 'is_credit', 'is_endorse', 'enabled'], 'integer'],
            [['insurance_amount', 'office_amount', 'adc_method',], 'double'],
            [['create_date'], 'safe'],
            [['concept'], 'string'],
            [['id_user_cashier'], 'exist', 'skipOnError' => false, 'targetClass' => User::className(), 'targetAttribute' => ['id_user_cashier' => 'id']],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
            [['id_dealer'], 'exist', 'skipOnError' => false, 'targetClass' => Dealer::className(), 'targetAttribute' => ['id_dealer' => 'id']],
            [['type_transaction'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentStatus::className(), 'targetAttribute' => ['type_transaction' => 'id']],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_agency'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['id_agency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_agency' => 'Id Agency',
            'id_policy' => 'Id Policy',
            'id_customer' => 'Id Customer',
            'id_user' => 'Id User',
            'id_user_cashier' => 'Id User Cashier',
            'id_dealer' => 'Id Dealer',
            'type_transaction' => 'Type Transaction',
            'create_date' => 'Create Date',
            'ins_method' => 'Ins Method',
            'insurance_amount' => 'Insurance Amount',
            'office_amount' => 'Office Amount',
            'adc_method' => 'Adc Method',
            'adc_amount' => 'Adc Amount',
            'is_credit' => 'Is Credit',
            'is_endorse' => 'Is Endorse',
            'concept' => 'Concept',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCashier()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user_cashier']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealer()
    {
        return $this->hasOne(Dealer::className(), ['id' => 'id_dealer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeTransaction()
    {
        return $this->hasOne(PaymentStatus::className(), ['id' => 'type_transaction']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy'])->select('id,policy_number,id_user,id_company,id_customer')
            ->with('customer')
            ->with('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'id_agency']);
    }
    public function getSuma()
    {
        $this->select('SUM(insurance_amount,office_amount)');
    }
}
