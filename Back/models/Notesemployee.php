<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notesemployee".
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $note
 * @property string $create_date
 * @property int $enabled
 *
 * @property User $user
 */
class Notesemployee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notesemployee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'create_date'], 'required'],
            [['id_user', 'enabled'], 'integer'],
            [['note'], 'string'],
            [['create_date'], 'safe'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'note' => 'Note',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->select("id,full_name");
    }
}
