<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property int|null $id_llc
 * @property string $name
 * @property string $site_url
 * @property string $phone
 * @property string $create_date
 * @property int $enabled
 *
 * @property LlcTypes $llc
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_llc', 'enabled'], 'integer'],
            [['name', 'site_url', 'phone', 'create_date', 'enabled'], 'required'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['site_url'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 15],
            [['id_llc'], 'exist', 'skipOnError' => true, 'targetClass' => LlcTypes::className(), 'targetAttribute' => ['id_llc' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_llc' => 'Id Llc',
            'name' => 'Name',
            'site_url' => 'Site Url',
            'phone' => 'Phone',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlc()
    {
        return $this->hasOne(LlcTypes::className(), ['id' => 'id_llc'])->select("id,name");
    }
}
