<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "types_file".
 *
 * @property int $id
 * @property string $name
 * @property string $create_date
 * @property int $enabled
 *
 * @property FilesPolicy[] $filesPolicies
 */
class TypesFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'types_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'create_date'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilesPolicies()
    {
        return $this->hasMany(FilesPolicy::className(), ['id_type_file' => 'id']);
    }
}
