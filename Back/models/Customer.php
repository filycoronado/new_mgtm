<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_agency
 * @property string|null $name
 * @property string|null $last_name
 * @property string|null $address
 * @property string|null $state
 * @property string|null $zipcode
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $home_phone
 * @property string|null $work_phone
 * @property int|null $gender
 * @property string $client_number
 * @property int $enabled
 * @property string|null $access_token
 *
 * @property User $user
 * @property Agency $agency
 * @property Policy[] $policies
 * @property Tickets[] $tickets
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_agency', 'client_number'], 'required'],
            [['id_user', 'id_agency', 'gender', 'enabled'], 'integer'],
            [['access_token'], 'string'],
            [['name', 'last_name', 'address'], 'string', 'max' => 150],
            [['state'], 'string', 'max' => 25],
            [['zipcode'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 200],
            [['phone', 'home_phone', 'work_phone'], 'string', 'max' => 15],
            [['client_number'], 'string', 'max' => 20],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_agency'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['id_agency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_agency' => 'Id Agency',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'state' => 'State',
            'zipcode' => 'Zipcode',
            'email' => 'Email',
            'phone' => 'Phone',
            'home_phone' => 'Home Phone',
            'work_phone' => 'Work Phone',
            'gender' => 'Gender',
            'client_number' => 'Client Number',
            'enabled' => 'Enabled',
            'access_token' => 'Access Token',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_agency'])->select("id,name,code");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'id_user'])->select("id,full_name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicies()
    {
        return $this->hasMany(Policy::className(), ['id_customer' => 'id'])->with("company")->with("status0")->with("subStatus")->with("user")->with("policyType");
    }
}
