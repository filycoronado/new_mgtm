<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property int $id_agency
 * @property string|null $username
 * @property string|null $password
 * @property string|null $full_name
 * @property string|null $create_date
 * @property string|null $dob
 * @property int $gender
 * @property string $role
 * @property string|null $token
 * @property int $is_cashier
 * @property int $in_login
 * @property int|null $enabled
 *
 * @property Permisions[] $permisions
 * @property Agency $agency
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_agency', 'gender', 'role', 'is_cashier', 'in_login'], 'required'],
            [['id_agency', 'gender', 'is_cashier', 'in_login', 'enabled'], 'integer'],
            [['password', 'token'], 'string'],
            [['create_date', 'dob'], 'safe'],
            [['username'], 'string', 'max' => 150],
            [['full_name'], 'string', 'max' => 200],
            [['role'], 'string', 'max' => 30],
            [['id_agency'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['id_agency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_agency' => 'Id Agency',
            'username' => 'Username',
            'password' => 'Password',
            'full_name' => 'Full Name',
            'create_date' => 'Create Date',
            'dob' => 'Dob',
            'gender' => 'Gender',
            'role' => 'Role',
            'token' => 'Token',
            'is_cashier' => 'Is Cashier',
            'in_login' => 'In Login',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermisions()
    {
        return $this->hasMany(Permisions::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'id_agency'])->select("id,name,code");
    }
}
