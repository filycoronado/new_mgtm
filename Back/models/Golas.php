<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "golas".
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $name
 * @property int|null $nb
 * @property int|null $nb_adc
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string|null $create_date
 * @property int $enabled
 *
 * @property User $user
 */
class Golas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'golas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'nb', 'nb_adc', 'enabled'], 'integer'],
            [['start_date', 'end_date', 'create_date'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'name' => 'Name',
            'nb' => 'Nb',
            'nb_adc' => 'Nb Adc',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
