<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "policy_sub_status".
 *
 * @property int $id
 * @property int $id_policystatus
 * @property string|null $name
 * @property string|null $create_date
 * @property int|null $enabled
 *
 * @property Statuspolicy $policystatus
 */
class PolicySubStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'policy_sub_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_policystatus'], 'required'],
            [['id_policystatus', 'enabled'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['id_policystatus'], 'exist', 'skipOnError' => true, 'targetClass' => Statuspolicy::className(), 'targetAttribute' => ['id_policystatus' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_policystatus' => 'Id Policystatus',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicystatus()
    {
        return $this->hasOne(Statuspolicy::className(), ['id' => 'id_policystatus']);
    }
}
