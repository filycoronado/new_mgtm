<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vin".
 *
 * @property int $id
 * @property int $id_policy
 * @property string $vin_number
 * @property string $create_date
 * @property int $enabled
 *
 * @property Policy $policy
 */
class Vin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_policy', 'vin_number', 'create_date'], 'required'],
            [['id_policy', 'enabled'], 'integer'],
            [['create_date'], 'safe'],
            [['vin_number'], 'string', 'max' => 25],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_policy' => 'Id Policy',
            'vin_number' => 'Vin Number',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy'])->select('id,policy_number');
    }
}
