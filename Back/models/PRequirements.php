<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "p_requirements".
 *
 * @property int $id
 * @property string $name
 * @property string $create_date
 * @property int $enabled
 *
 * @property RequerimentsPolicy[] $requerimentsPolicies
 */
class PRequirements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'p_requirements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'create_date', 'enabled'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimentsPolicies()
    {
        return $this->hasMany(RequerimentsPolicy::className(), ['id_requeriment' => 'id']);
    }
}
