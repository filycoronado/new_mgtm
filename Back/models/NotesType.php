<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notes_type".
 *
 * @property int $id
 * @property string $name
 * @property string $create_date
 * @property int $enabled
 *
 * @property Notes[] $notes
 */
class NotesType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notes_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'create_date'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(Notes::className(), ['id_note_type' => 'id']);
    }
}
