<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property int $id
 * @property int $id_policy
 * @property int $id_customer
 * @property string|null $card_number
 * @property string|null $card_exp
 * @property string|null $back_number
 * @property string|null $route_number
 * @property string|null $account_number
 * @property string $create_date
 * @property int $enabled
 *
 * @property Policy $policy
 * @property Customer $customer
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_policy', 'id_customer', 'create_date'], 'required'],
            [['id_policy', 'id_customer', 'enabled'], 'integer'],
            [['create_date'], 'safe'],
            [['card_number', 'route_number', 'account_number'], 'string', 'max' => 25],
            [['card_exp'], 'string', 'max' => 5],
            [['back_number'], 'string', 'max' => 6],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_policy' => 'Id Policy',
            'id_customer' => 'Id Customer',
            'card_number' => 'Card Number',
            'card_exp' => 'Card Exp',
            'back_number' => 'Back Number',
            'route_number' => 'Route Number',
            'account_number' => 'Account Number',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer'])->select("id,name,last_name");
    }
}
