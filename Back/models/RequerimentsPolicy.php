<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requeriments_policy".
 *
 * @property int $id
 * @property int $id_requeriment
 * @property int $id_policy
 * @property string $status_requerinments
 * @property string $create_date
 * @property int $enabled
 *
 * @property PRequirements $requeriment
 * @property Policy $policy
 */
class RequerimentsPolicy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requeriments_policy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_requeriment', 'id_policy', 'status_requerinments', 'create_date'], 'required'],
            [['id_requeriment', 'id_policy', 'enabled'], 'integer'],
            [['create_date'], 'safe'],
            [['status_requerinments'], 'string', 'max' => 30],
            [['id_requeriment'], 'exist', 'skipOnError' => true, 'targetClass' => PRequirements::className(), 'targetAttribute' => ['id_requeriment' => 'id']],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_requeriment' => 'Id Requeriment',
            'id_policy' => 'Id Policy',
            'status_requerinments' => 'Status Requerinments',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequeriment()
    {
        return $this->hasOne(PRequirements::className(), ['id' => 'id_requeriment'])->select('id,name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy'])->select('id,policy_number,id_customer')->with('customer');
    }
}
