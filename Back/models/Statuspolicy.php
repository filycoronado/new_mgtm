<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statuspolicy".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $create_date
 * @property int|null $enabled
 *
 * @property PolicySubStatus[] $policySubStatuses
 */
class Statuspolicy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statuspolicy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicySubStatuses()
    {
        return $this->hasMany(PolicySubStatus::className(), ['id_policystatus' => 'id']);
    }
}
