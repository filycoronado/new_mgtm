<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agency".
 *
 * @property int $id
 * @property int $code 
 * 
 * @property string|null $name
 * @property string|null $create_date
 * @property int|null $enabled
 *
 * @property User[] $users
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code', 'enabled'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 90],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_agency' => 'id']);
    }
}
