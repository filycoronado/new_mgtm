<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files_policy".
 *
 * @property int $id
 * @property int $id_policy
 * @property int $id_user
 * @property int|null $id_type_file
 * @property string $file
 * @property string $create_date
 * @property int $enabled
 *
 * @property Policy $policy
 * @property User $user
 * @property TypesFile $typeFile
 */
class FilesPolicy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files_policy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_policy', 'id_user', 'file', 'create_date'], 'required'],
            [['id_policy', 'id_user', 'id_type_file', 'enabled'], 'integer'],
            [['file'], 'string'],
            [['create_date'], 'safe'],
            [['id_policy'], 'exist', 'skipOnError' => true, 'targetClass' => Policy::className(), 'targetAttribute' => ['id_policy' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_type_file'], 'exist', 'skipOnError' => true, 'targetClass' => TypesFile::className(), 'targetAttribute' => ['id_type_file' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_policy' => 'Id Policy',
            'id_user' => 'Id User',
            'id_type_file' => 'Id Type File',
            'file' => 'File',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(Policy::className(), ['id' => 'id_policy'])->select('id,policy_number');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->select("id,full_name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeFile()
    {
        return $this->hasOne(TypesFile::className(), ['id' => 'id_type_file'])->select("id,name");
    }
}
