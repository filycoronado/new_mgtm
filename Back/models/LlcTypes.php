<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "llc_types".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $create_date
 * @property int|null $enabled
 *
 * @property Companies[] $companies
 */
class LlcTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'llc_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['id_llc' => 'id']);
    }
}
