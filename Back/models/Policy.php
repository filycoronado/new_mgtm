<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "policy".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_customer
 * @property int $id_agency
 * @property int $id_llc
 * @property int $id_policy_type
 * @property int $id_company
 * @property int $id_dealer
 * @property int $status
 * @property int $id_sub_status
 * @property string $policy_number
 * @property string $membership_number
 * @property string $effective_date
 * @property string $cancelation_date
 * @property string $expiration_date
 * @property string $pending_date
 * @property string $due_date
 * @property string $create_date
 * @property string $delivery_seller
 * @property string $delivery_name
 * @property int $enabled
 *
 * @property User $user
 * @property Agency $agency
 * @property Customer $customer
 * @property LlcTypes $llc
 * @property PolicyTypes $policyType
 * @property Companies $company
 * @property Statuspolicy $status0
 * @property PolicySubStatus $subStatus
 */
class Policy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'policy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_customer', 'id_agency', 'id_llc', 'id_policy_type', 'id_company', 'id_dealer', 'status', 'id_sub_status', 'policy_number', 'membership_number', 'effective_date', 'cancelation_date', 'expiration_date', 'pending_date', 'due_date', 'create_date', 'delivery_seller', 'delivery_name'], 'required'],
            [['id_user', 'id_customer', 'id_agency', 'id_llc', 'id_policy_type', 'id_company', 'id_dealer', 'status', 'id_sub_status', 'enabled'], 'integer'],
            [['effective_date', 'cancelation_date', 'expiration_date', 'pending_date', 'due_date', 'create_date'], 'safe'],
            [['policy_number', 'membership_number'], 'string', 'max' => 20],
            [['delivery_seller', 'delivery_name'], 'string', 'max' => 150],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_agency'], 'exist', 'skipOnError' => true, 'targetClass' => Agency::className(), 'targetAttribute' => ['id_agency' => 'id']],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
            [['id_llc'], 'exist', 'skipOnError' => true, 'targetClass' => LlcTypes::className(), 'targetAttribute' => ['id_llc' => 'id']],
            [['id_policy_type'], 'exist', 'skipOnError' => true, 'targetClass' => PolicyTypes::className(), 'targetAttribute' => ['id_policy_type' => 'id']],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['id_company' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Statuspolicy::className(), 'targetAttribute' => ['status' => 'id']],
            [['id_sub_status'], 'exist', 'skipOnError' => true, 'targetClass' => PolicySubStatus::className(), 'targetAttribute' => ['id_sub_status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_customer' => 'Id Customer',
            'id_agency' => 'Id Agency',
            'id_llc' => 'Id Llc',
            'id_policy_type' => 'Id Policy Type',
            'id_company' => 'Id Company',
            'id_dealer' => 'Id Dealer',
            'status' => 'Status',
            'id_sub_status' => 'Id Sub Status',
            'policy_number' => 'Policy Number',
            'membership_number' => 'Membership Number',
            'effective_date' => 'Effective Date',
            'cancelation_date' => 'Cancelation Date',
            'expiration_date' => 'Expiration Date',
            'pending_date' => 'Pending Date',
            'due_date' => 'Due Date',
            'create_date' => 'Create Date',
            'delivery_seller' => 'Delivery Seller',
            'delivery_name' => 'Delivery Name',
            'enabled' => 'Enabled',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user'])->select("id,full_name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'id_agency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer'])->select('id,name,last_name,address,state,zipcode');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlc()
    {
        return $this->hasOne(LlcTypes::className(), ['id' => 'id_llc']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicyType()
    {
        return $this->hasOne(PolicyTypes::className(), ['id' => 'id_policy_type'])->select("id,name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'id_company'])->select("id,name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Statuspolicy::className(), ['id' => 'status'])->select("id,name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubStatus()
    {
        return $this->hasOne(PolicySubStatus::className(), ['id' => 'id_sub_status'])->select("id,name");
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Tickets::className(), ['id_policy' => 'id']);
    }


     /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(FilesPolicy::className(), ['id_policy' => 'id']);
    }
}
