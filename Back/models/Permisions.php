<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permisions".
 *
 * @property int $id
 * @property int $id_user
 * @property int $admin
 * @property int $employees
 * @property int $agencies
 * @property int $companies
 * @property int $dealers
 * @property int|null $marketing
 * @property int $map
 * @property int $mapReport
 * @property int $editNote
 * @property int $add_status
 * @property int $edit_payment
 * @property int $edit_status 
 * @property int $view_cc 
 * @property User $user
 */
class Permisions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permisions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'admin', 'employees', 'agencies', 'companies', 'dealers', 'marketing', 'map', 'mapReport', 'edit_note', 'add_status', 'edit_payment', 'edit_status', 'view_cc'], 'integer'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'admin' => 'Admin',
            'employees' => 'Employees',
            'agencies' => 'Agencies',
            'companies' => 'Companies',
            'dealers' => 'Dealers',
            'marketing' => 'Marketing',
            'map' => 'Map',
            'mapReport' => 'Map Report',
            'edit_note' => 'Edit Note',
            'add_status' => 'Add Status',
            'edit_payment' => 'Edit Payment',
            'edit_status' => 'Edit Status',
            'view_cc' => 'View CC'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
