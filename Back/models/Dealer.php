<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dealer".
 *
 * @property int $id
 * @property string $name
 * @property string|null $location
 * @property string|null $zone
 * @property string $create_date
 * @property int $enabled
 */
class Dealer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dealer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'create_date', 'enabled'], 'required'],
            [['create_date'], 'safe'],
            [['enabled'], 'integer'],
            [['name'], 'string', 'max' => 80],
            [['location'], 'string', 'max' => 140],
            [['zone'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'location' => 'Location',
            'zone' => 'Zone',
            'create_date' => 'Create Date',
            'enabled' => 'Enabled',
        ];
    }
}
