-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-12-2019 a las 22:08:46
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ins_mgtm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `name` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `agency`
--

INSERT INTO `agency` (`id`, `code`, `name`, `create_date`, `enabled`) VALUES
(1, 101, 'SOUTH LOCATION', '2019-12-17', 1),
(2, 103, 'EAST SIDE LOCATION', '2019-12-12', 1),
(3, 105, 'HERMOSILLO LOCATION', '2019-12-12', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `id_llc` int(11) DEFAULT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `site_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `enabled` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `id_llc`, `name`, `site_url`, `phone`, `create_date`, `enabled`) VALUES
(1, 1, 'HALLMARK', 'https://www.hallmarkgrp.com/', '5204452088', '2019-12-13', 1),
(2, 1, 'GAINSCO', 'https://www.gainsco.com/', '5204452088', '2019-12-13', 1),
(3, 1, 'PROGRESSIVE', 'https://www.progressive.com/home/home/', '5204452088', '2019-12-13', 1),
(4, 2, 'FOREMOST', 'WWW.FOREMOST.COM', '5204452088', '2019-12-28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `client_number` int(11) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dealer`
--

CREATE TABLE `dealer` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(140) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date NOT NULL,
  `enabled` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `dealer`
--

INSERT INTO `dealer` (`id`, `name`, `location`, `zone`, `create_date`, `enabled`) VALUES
(1, 'CHAPMAN-PAWVERDE', '32.1560733,-110.973574', 'NORTH', '2019-12-13', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `golas`
--

CREATE TABLE `golas` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nb` int(11) DEFAULT NULL,
  `nb_adc` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `golas`
--

INSERT INTO `golas` (`id`, `id_user`, `name`, `nb`, `nb_adc`, `start_date`, `end_date`, `create_date`, `enabled`) VALUES
(2, 1, 'DECEMBER GOAL', 15, 15, '2019-12-01', '2019-12-31', '2019-12-18', 1),
(3, 1, 'Enero', 25, 25, '2020-01-01', '2020-01-31', '2019-12-20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llc_types`
--

CREATE TABLE `llc_types` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `llc_types`
--

INSERT INTO `llc_types` (`id`, `name`, `create_date`, `enabled`) VALUES
(1, 'OLDTOWN', '2019-12-26', 1),
(2, 'CVA', '2019-12-26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notesemployee`
--

CREATE TABLE `notesemployee` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `notesemployee`
--

INSERT INTO `notesemployee` (`id`, `id_user`, `note`, `create_date`, `enabled`) VALUES
(1, 1, '<p><i><strong>Hello test note &nbsp; updated 23265465465654</strong></i></p><p><i><strong>65564654654</strong></i></p>', '2019-12-17', 1),
(2, 1, '<h2>hello test note 2</h2>', '2019-12-17', 1),
(3, 1, '<ol><li>test</li><li>list</li></ol>', '2019-12-17', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisions`
--

CREATE TABLE `permisions` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT 0,
  `employees` tinyint(4) NOT NULL DEFAULT 0,
  `agencies` tinyint(4) NOT NULL DEFAULT 0,
  `companies` tinyint(4) NOT NULL DEFAULT 0,
  `dealers` tinyint(4) NOT NULL DEFAULT 0,
  `marketing` tinyint(4) DEFAULT 1,
  `map` tinyint(4) NOT NULL DEFAULT 0,
  `mapReport` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permisions`
--

INSERT INTO `permisions` (`id`, `id_user`, `admin`, `employees`, `agencies`, `companies`, `dealers`, `marketing`, `map`, `mapReport`) VALUES
(1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
(2, 3, 1, 1, 1, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policies_status`
--

CREATE TABLE `policies_status` (
  `id` int(11) NOT NULL,
  `name` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `policies_status`
--

INSERT INTO `policies_status` (`id`, `name`, `create_date`, `enabled`) VALUES
(1, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `policy_types`
--

CREATE TABLE `policy_types` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `policy_types`
--

INSERT INTO `policy_types` (`id`, `name`, `create_date`, `enabled`) VALUES
(1, 'AUTO', '2019-12-26', 1),
(2, 'HOME', '2019-12-26', 1),
(3, 'LIFE', '2019-12-26', 1),
(4, 'MOTORCYCLE', '2019-12-26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_agency` int(11) NOT NULL,
  `username` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `token` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_cashier` tinyint(4) NOT NULL,
  `in_login` tinyint(4) NOT NULL,
  `enabled` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `id_agency`, `username`, `password`, `full_name`, `create_date`, `dob`, `gender`, `role`, `token`, `is_cashier`, `in_login`, `enabled`) VALUES
(1, 1, 'USERFILY', '81dc9bdb52d04dc20036dbd8313ed055', 'FILIBERTO CORONADO', '2019-12-19', '1992-01-02', 1, 'ADMIN', '05120159090856ce048b67990817b65945e1809b', 0, 0, 1),
(3, 3, 'ALBERTAGENT', '81dc9bdb52d04dc20036dbd8313ed055', 'RAMON ALBERTO MENDOZA EL TREMENDO', '2019-12-13', '1991-01-01', 1, 'ADMIN', NULL, 0, 0, 1),
(4, 3, 'USERROFOLDO', '81dc9bdb52d04dc20036dbd8313ed055', 'RODOLFO CONTRERAS', '2019-12-13', '2019-12-11', 1, 'AGENT', NULL, 0, 0, 1),
(5, 1, 'ADMINDEMO', '81dc9bdb52d04dc20036dbd8313ed055', 'ADMINDEMO', '2019-12-21', '2019-12-21', 1, 'ADMIN', NULL, 0, 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_agency` (`code`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_llc` (`id_llc`);

--
-- Indices de la tabla `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dealer`
--
ALTER TABLE `dealer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `golas`
--
ALTER TABLE `golas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `llc_types`
--
ALTER TABLE `llc_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notesemployee`
--
ALTER TABLE `notesemployee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `permisions`
--
ALTER TABLE `permisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `policies_status`
--
ALTER TABLE `policies_status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `policy_types`
--
ALTER TABLE `policy_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_agency` (`id_agency`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dealer`
--
ALTER TABLE `dealer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `golas`
--
ALTER TABLE `golas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `llc_types`
--
ALTER TABLE `llc_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `notesemployee`
--
ALTER TABLE `notesemployee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `permisions`
--
ALTER TABLE `permisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `policies_status`
--
ALTER TABLE `policies_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `policy_types`
--
ALTER TABLE `policy_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_ibfk_1` FOREIGN KEY (`id_llc`) REFERENCES `llc_types` (`id`);

--
-- Filtros para la tabla `golas`
--
ALTER TABLE `golas`
  ADD CONSTRAINT `golas_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `notesemployee`
--
ALTER TABLE `notesemployee`
  ADD CONSTRAINT `notesemployee_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `permisions`
--
ALTER TABLE `permisions`
  ADD CONSTRAINT `permisions_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_agency`) REFERENCES `agency` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
