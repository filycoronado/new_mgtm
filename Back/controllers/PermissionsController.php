<?php

namespace app\controllers;

use app\models\Agency;
use app\models\Permisions;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class PermissionsController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGet_permission_user($id)
    {
        $item = Permisions::find()->where("id_user=" . $id)->one();

        if ($item != null) {
            $response = [
                "status" => "success",
                "data" => $item,
            ];
        } else {
            $response = [
                "status" => "error",
                "data" => $item,
            ];
        }
        return $response;
    }

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $id_user = $request->post("id_user");
        $id_permision = $request->post("id_permision");
        $value = $request->post("value");
        $item = Permisions::find()->where("id_user=" . $id_user)->one();
        if ($item != null) {
            if ($this->update_permision($id_permision, $item, $value)) {
                $response = [
                    "status" => "success",
                    "message" => "Updated Successfully,"
                ];
            } else {
                $response = [
                    "status" => "error",
                    "message" => "Updated Error,"
                ];
            }
        } else {
            if ($this->save_permision($id_permision, $id_user, $value)) {
                $response = [
                    "status" => "success",
                    "message" => "Saved Successfully,"
                ];
            } else {
                $response = [
                    "status" => "error",
                    "message" => "Saved Error,"
                ];
            }
        }

        return $response;
    }
    private function save_permision($id_permision, $id_user, $value)
    {
        $item = new Permisions();
        $item->id_user = $id_user;
        switch ($id_permision) {
            case 1: //admin
                $item->admin = $value;
                break;
            case 2: // employee
                $item->employees = $value;
                break;
            case 3: //agencies
                $item->agencies = $value;
                break;
            case 4: //companies
                $item->companies = $value;
                break;
            case 5: //Dealer
                $item->dealers = $value;
                break;
            case 6: //Status
                $item->add_status = $value;
                break;
            case 7: //edit Status
                $item->edit_status = $value;
                break;
            case 8: //edit_note
                $item->edit_note = $value;
                break;
            case 9: //edit payment
                $item->edit_payment = $value;
                break;
        }

        if ($item->save(false)) {
            return true;
        } else {
            return false;
        }
    }
    private function update_permision($id_permision, Permisions $item, $value)
    {
        switch ($id_permision) {
            case 1: //admin
                $item->admin = $value;
                break;
            case 2: // employee
                $item->employees = $value;
                break;
            case 3: //agencies
                $item->agencies = $value;
                break;
            case 4: //companies
                $item->companies = $value;
                break;
            case 5: //Dealer
                $item->dealers = $value;
                break;
            case 6: //Status
                $item->add_status = $value;
                break;
            case 7: //edit Status
                $item->edit_status = $value;
                break;
            case 8: //edit_note
                $item->edit_note = $value;
                break;
            case 9: //edit payment
                $item->edit_payment = $value;
                break;
        }

        if ($item->update(false)) {
            return true;
        } else {
            return false;
        }
    }
}
