<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\Customer;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;
use yii\db\Command;


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class CustomerController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        return Agency::find()->where("enabled=1")->all();
    }

    public function actionSave()
    {
        $flagEmail = false;
        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new  Customer();
        $model->attributes = $data;
        $model->enabled = 0;
        $model->client_number = $this->random_strings(5);
        /*  if ($model->email != "") { //"layouts/html", ['content' => "verify"]
            $mailer = Yii::$app->mailer->compose() // el resultado del renderizado de la vista se transforma en el cuerpo del mensaje aquí
                ->setFrom('dontReply@buylowins.com')
                ->setTo("7e259a7ccd-71591e@inbox.mailtrap.io")
                ->setSubject('Asunto del mensaje');
            if ($mailer->send()) {
                $flagEmail = true;
                $response = [
                    "status" => "success",
                    "message" => "valid Mail"
                ];
            }
        }*/

        $flagEmail = true;
        if ($flagEmail == false) {
            $response = [
                "status" => "error",
                "message" => "Invalid mail please add an existing mail."
            ];
        } else {

            if ($model->save(false)) {


                $response = [
                    "status" => "success",
                    "message" => "Customer Saved Successfully.",
                    "customer" => $model
                ];
            } else {
                $response = [
                    "status" => "error",
                    "message" => "Customer Can´t Save."
                ];
            }
        }
        return $response;
    }


    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Customer::findOne($id);
        $model->attributes = $data;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Customer Updated Successfully.",
                "customer" => $model,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Customer Dont Updated.",
            ];
        }
        return $response;
    }
    public function actionGet_by_id($id)
    {
        $customer = Customer::findOne($id);
        if ($customer != null) {
            $response = [
                "status" => "success",
                "message" => "Customer Found.",
                "data" => $customer,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Customer Not Found.",
            ];
        }
        return $response;
    }

    function random_strings($length_of_string)
    {
        // String of all alphanumeric character 
        $str_result = '0123456789';
        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function actionSerach()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $name = '';
        $last_name = '';
        $customer_number = '';
        $policy_number = '';

        if ($data['name'] != '') {
            $name = "name LIKE '%" . $data['name'] . "%'";
        }

        if ($data['last_name'] != '') {
            $last_name = "last_name LIKE '%" . $data['last_name'] . "%'";
        }

        if ($data['customer_number'] != '') {
            $customer_number = "client_number=" . $data['customer_number'];
        }

        if ($data['policy_number'] != '') {
            $policy_number = "policy_number=" . $data['policy_number'];
        }

        $customers = Customer::find()

            ->joinWith("policies")
            ->where($name)
            ->andWhere($last_name)
            ->andWhere($customer_number)
            ->andwhere($policy_number)
            ->asArray()
            ->all();


        //return  var_dump($customers->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        if ($customers != null) {
            $response = [
                'status' => 'success',
                'message' => 'clients found',
                'customers' => $customers
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'clients not found'

            ];
        }
        return $response;
    }
}
