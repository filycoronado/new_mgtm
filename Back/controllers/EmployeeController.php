<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class EmployeeController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionLogin()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $username = $data['username'];
        $password = md5($data['password']);
        $agency = $data['id_agency'];

        $user = User::find()
            ->where("username='" . $username . "'")
            ->andWhere("password='" . $password . "'")
            ->one();
        $agency = Agency::find()
            ->where("code=" . $agency)
            ->one();
        //$length = 78 etc
        $token = bin2hex(random_bytes(20));
        if ($agency != null) {
            if ($user->id_agency == $agency->id) {
                //Success agency
                if ($user != null) {
                    $user->token = $token;
                    $user->update(false);
                    return $response = [
                        "status" => "success",
                        "message" => "Login Success",
                        "user" => $user,
                    ];
                } else {
                    return $response = [
                        "status" => "error",
                        "message" => "Login Error Credentials",
                    ];
                }
            } else {
                //error agency
                return $response = [
                    "status" => "error",
                    "message" => "Login Error Credentials",
                ];
            }
        }
        return $response;
    }



    public function actionGet_available_users()
    {
        $list = User::find()->select("id,full_name,username,dob,create_date,role,id_agency")
            ->where("enabled=1")
            ->with('agency')
            ->asArray()
            ->all();
        if ($list != null) {
            //success
            $response = [
                "status" => "success",
                "message" => "List Employees",
                "employees" => $list
            ];
        } else {
            //error
            $response = [
                "status" => "error",
                "message" => "List Employees Empty",
                "employees" => null,
            ];
        }
        return $response;
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $dob = date($data['dob']['year'] . "-" . $data['dob']['month'] . "-" . $data['dob']['day']);
        $role = "AGENT";
        if ($data['role'] == 1) {
            $role = "ADMIN";
        }
        $model = new User();
        $model->attributes = $data;
        $model->dob = $dob;
        $model->create_date = date("Y-m-d");
        $model->password = md5("1234");
        $model->role = $role;
        $model->is_cashier = $data['is_cashier'];
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Employee Saved",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Employee Dont Saved",
            ];
        }
        return  $response;
    }


    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $dob = date($data['dob']['year'] . "-" . $data['dob']['month'] . "-" . $data['dob']['day']);
        $role = "AGENT";
        if ($data['role'] == 1) {
            $role = "ADMIN";
        }
        $model = User::findOne($id);
        $model->attributes = $data;
        $model->dob = $dob;
        $model->create_date = date("Y-m-d");
        $model->is_cashier = $data['is_cashier'];




        // $model->password = md5("1234");
        $model->role = $role;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Employee Updated",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Employee Dont Updated",
            ];
        }
        return  $response;
    }

    public function actionGet_by_id($id)
    {
        $employee = User::findOne($id);
        if ($employee != null) {
            $response = [
                "status" => "success",
                "data" => $employee,
            ];
        } else {
            $response = [
                "status" => "Error",
                "data" => $employee,
            ];
        }

        return $response;
    }

    public function actionGet_cashiers()
    {

        return User::find()
            ->where("enabled=1")
            ->andWhere("is_cashier=1")
            ->all();
    }
}
