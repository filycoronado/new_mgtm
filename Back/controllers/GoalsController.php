<?php

namespace app\controllers;

use app\models\Agency;
use app\models\Golas;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class GoalsController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGet_goals($id)
    {
        $list = Golas::find()
            ->where("id_user=" . $id)
            ->where("enabled=1")
            ->with('user')
            ->asArray()
            ->all();
        if ($list != null) {
            //success
            $response = [
                "status" => "success",
                "message" => "List Goals",
                "goals" => $list
            ];
        } else {
            //error
            $response = [
                "status" => "error",
                "message" => "List Goals Empty",
                "goals" => null,
            ];
        }
        return $response;
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new Golas();
        $model->attributes = $data;
        $model->create_date = date("Y-m-d");
        $start_date = date($data['start_date']['year'] . "-" . $data['start_date']['month'] . "-" . $data['start_date']['day']);
        $end_date = date($data['end_date']['year'] . "-" . $data['end_date']['month'] . "-" . $data['end_date']['day']);
        $model->start_date = $start_date;
        $model->end_date = $end_date;
        $model->enabled = 1;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Goal Saved Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Goal Dont Saved.",
            ];
        }
        return $response;
    }
    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Golas::findOne($id);
        $model->attributes = $data;
        $start_date = date($data['start_date']['year'] . "-" . $data['start_date']['month'] . "-" . $data['start_date']['day']);
        $end_date = date($data['end_date']['year'] . "-" . $data['end_date']['month'] . "-" . $data['end_date']['day']);
        $model->start_date = $start_date;
        $model->end_date = $end_date;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Goal Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Goal Dont Updated.",
            ];
        }
        return $response;
    }

    public function actionGet_by_id($id)
    {
        $Golas = Golas::findOne($id);
        if ($Golas != null) {
            $response = [
                "status" => "success",
                "message" => "Goal Found.",
                "data" => $Golas,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Goal Not Found.",
            ];
        }
        return $response;
    }
}
