<?php

namespace app\controllers;

use app\models\Agency;
use app\models\Customer;
use app\models\Policy;
use app\models\Tickets;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class ApiController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function actionTest()
    {
        return "ajua";
    }

    public function actionLogin()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $username = $data['username'];
        $password = md5($data['password']);
        $agency = $data['id_agency'];

        $user = User::find()
            ->where("username='" . $username . "'")
            ->andWhere("password='" . $password . "'")
            ->one();
        $agency = Agency::find()
            ->where("code=" . $agency)
            ->one();
        //$length = 78 etc
        $token = bin2hex(random_bytes(20));
        if ($agency != null) {
            if ($user->id_agency == $agency->id) {
                //Success agency
                if ($user != null) {
                    $user->token = $token;
                    $user->update(false);
                    return $response = [
                        "status" => "success",
                        "message" => "Login Success",
                        "user" => $user,
                    ];
                } else {
                    return $response = [
                        "status" => "error",
                        "message" => "Login Error Credentials",
                    ];
                }
            } else {
                //error agency
                return $response = [
                    "status" => "error",
                    "message" => "Login Error Credentials",
                ];
            }
        }
        return $response;
    }



    public function actionGet_available_users()
    {
        $list = User::find()->select("full_name,username,dob,create_date,role,id_agency")
            ->where("enabled=1")
            ->with('agency')
            ->asArray()
            ->all();
        if ($list != null) {
            //success
            $response = [
                "status" => "success",
                "message" => "List Employees",
                "employees" => $list
            ];
        } else {
            //error
            $response = [
                "status" => "error",
                "message" => "List Employees Empty",
                "employees" => null,
            ];
        }
        return $response;
    }

    public function actionCustomer_login()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $client_number = $data['client_number'];
        $phone = $data['phone'];

        $customer   =  Customer::find()
            ->where("phone='" . $phone . "'")
            ->andWhere("client_number='" . $client_number . "'")
            ->andWhere('enabled=1')
            ->one();
        $token = bin2hex(random_bytes(20));
        if ($customer != null) {
            $customer->access_token = $token;
            if ($customer->update(false)) {
                $response = [
                    'status' => 'success',
                    'message' => 'Login Success',
                    'data' => $customer
                ];
            } else {
                $response = [
                    'status' => 'error',
                    'message' => 'Login Error Token',

                ];
            }
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Login Error Credentials',

            ];
        }

        return $response;
    }

    public function actionGetpolicies()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $access_token = $data['access_token'];
        $customer = Customer::find()
            ->where("access_token='" . $access_token . "'")
            ->one();


        if (!($customer != null)) {
            $response = [
                'status' => 'error',
                'message' => 'Invalid Token',

            ];
            return $response;
        }

        $policies = Policy::find()
            ->where("id_customer=" . $customer->id)
            ->andWhere("enabled=1")
            ->with('company')
            ->asArray()
            ->all();

        if ($policies != null) {
            $response = [
                'status' => 'success',
                'message' => 'Policies Found',
                'data' => $policies

            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Policies Not Found',

            ];
        }

        return $response;
    }
    public function actionGetpayments()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $access_token = $data['access_token'];
        $id_policy = $data['id_policy'];



        $customer = Customer::find()
            ->where("access_token='" . $access_token . "'")
            ->one();


        if (!($customer != null)) {
            $response = [
                'status' => 'error',
                'message' => 'Invalid Token',

            ];
            return $response;
        }


        $payments = Tickets::find()
            ->where("id_policy=" . $id_policy)
            ->andWhere("enabled=1")
            ->all();

        if ($payments != null) {
            $response = [
                'status' => 'success',
                'message' => 'Payments Found',
                'data' => $payments

            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Payments Not Found',

            ];
        }

        return $response;
    }

    public function actionGetfiles()
    {
    }
}
