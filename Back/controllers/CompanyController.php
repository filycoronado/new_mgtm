<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\Companies;

use Faker\Provider\ar_JO\Company;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class CompanyController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function actionGet_companies()
    {
        $list = Companies::find()
            ->where("enabled=1")
            ->with("llc")->asArray()
            ->all();
        if ($list != null) {
            $response = [
                "status" => "success",
                "companies" => $list
            ];
        } else {
            $response = [
                "status" => "Error",
                "companies" => null
            ];
        }

        return $response;
    }
    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new Companies();
        $model->attributes = $data;
        $model->create_date = date("Y-m-d");
        $model->enabled = 1;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Company Saved Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Company Dont Saved.",
            ];
        }
        return $response;
    }

    public function actionUpdate()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Companies::findOne($id);
        $model->attributes = $data;
        $model->enabled = 1;

        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Company Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Company Dont Updated.",
            ];
        }
        return $response;
    }

    public function actionGet_by_id($id)
    {
        $item = Companies::findOne($id);

        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Company Found.",
                "data" => $item,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Company Not Found.",
            ];
        }
        return $response;
    }

    public function actionGet_by_llc($id)
    {
        $list = Companies::find()
            ->where("enabled=1")
            ->andWhere("id_llc=" . $id)
            ->all();
        if ($list != null) {
            $response = [
                "status" => "success",
                "companies" => $list
            ];
        } else {
            $response = [
                "status" => "Error",
                "companies" => null
            ];
        }

        return $response;
    }
}
