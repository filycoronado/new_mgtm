<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\PRequirements;
use app\models\RequerimentsPolicy;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class Requirements_policyController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id_policy = $data['id_policy'];
        $items = RequerimentsPolicy::find()
            ->where("enabled=1")
            ->andWhere('id_policy=' . $id_policy)
            ->with('requeriment')
            ->with('policy')
            ->asArray()
            ->all();

        if ($items != null) {
            $response = [
                'status' => 'success',
                'message' => 'Items Found',
                'data' => $items
            ];
        } else {
            $response = [
                'status ' => 'error',
                'message' => 'Items Not Found'
            ];
        }

        return $response;
    }

    public function actionGet_requeriments()
    {
        $items = PRequirements::find()
            ->where("enabled=1")
            ->all();

        if ($items != null) {
            $response = [
                'status' => 'success',
                'message' => 'Items Found',
                'data' => $items
            ];
        } else {
            $response = [
                'status' => 'Error',
                'message' => 'Items Not Found'

            ];
        }
        return $response;
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new RequerimentsPolicy();
        $model->attributes = $data;
        $model->create_date = date("Y-m-d");
        $model->enabled = 1;

        $item = RequerimentsPolicy::find()
            ->where("id_policy=" . $model->id_policy)
            ->andWhere("id_requeriment=" . $model->id_requeriment)
            ->one();

        if ($item == null) {
            if ($model->save(false)) {
                $response = [
                    "status" => "success",
                    "message" => "Status Saved Successfully.",
                ];
            } else {
                $response = [
                    "status" => "Error",
                    "message" => "Status Dont Saved.",
                ];
            }
        } else {
            $response = [
                "status" => "Error",
                "message" => "Status Already Exist.",
            ];
        }

        return $response;
    }


    public function actionUpdate()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = RequerimentsPolicy::findOne($id);
        $model->attributes = $data;

        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Status Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Status Dont Updated.",
            ];
        }
        return $response;
    }

    public function actionGet_by_id($id)
    {
        $item = RequerimentsPolicy::findOne($id);

        if ($item != null) {
            $response = [
                'status' => 'success',
                'message' => 'Status Found',
                'data' => $item
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Status Not Found',
            ];
        }
        return $response;
    }
}
