<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\Customer;
use app\models\Historic;
use app\models\Policy;
use app\models\Tickets;
use Symfony\Component\BrowserKit\History;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;
use DateTime;
use inquid\pdf\FPDF;
use app\models\PDF;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class RetentionController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionRetention_control()
    {
        $allPolicies = Policy::find()
            ->where("enabled=1")
            ->andWhere('status!=3')
            ->andWhere("id_sub_status!=10")
            ->all();

        foreach ($allPolicies as $clave => $valor) {
            $date = date('Y-m-d');
            if ($valor->status == 1 &&  $valor->id_sub_status == 1 || $valor->status == 1 &&  $valor->id_sub_status == 2) { //Active-Active Active-New Business To pending
                $due_date = date("Y-m-d", strtotime($valor->due_date . "+ 1 days"));
                if ($due_date == $date) {
                    $valor->id_sub_status = 3;
                }
            }
            if ($valor->status == 1 &&  $valor->id_sub_status == 3) { //Active-Pending cancellation To inactive cancel dnp

                $due_date = date("Y-m-d", strtotime($valor->due_date . "+ 7 days"));
                if ($due_date == $date) {
                    $valor->status = 2;
                    $valor->id_sub_status = 6;
                }
            }
            if ($valor->status == 2 &&  $valor->id_sub_status == 6) { //Inactive-dnp To Inactive-Expired
                $cancelDate = date("Y-m-d", strtotime($valor->cancelation_date . "+ 1 days"));
                if ($cancelDate == $date) {
                    $valor->status = 2;
                    $valor->id_sub_status = 8;
                }
            }
            if ($valor->status == 2 &&  $valor->id_sub_status == 8) { //Inactive-exp To Inactive-inactive
                $due_date = date("Y-m-d", strtotime($valor->due_date . "+ 1 month"));
                if ($due_date == $date) {
                    $valor->status = 3;
                    $valor->id_sub_status = 10;
                }
            }
            if (!($valor->status == 3 &&  $valor->id_sub_status == 10)) { //chenge to pending renewal if policy is not inactive
                $renewaldate = date("Y-m-d", strtotime($valor->due_date . "- 15 days"));
                if ($renewaldate == $date) {
                    $valor->status = 3;
                    $valor->id_sub_status = 10;
                }
            }

            if ($valor->update(false)) {
                $this->SaveHistoric($valor);
            }
        }

        return "process end";
    }

    public function SaveHistoric(Policy $item)
    {
        if (!$this->Check_history($item)) {
            $history = new Historic();
            $history->id_policy = $item->id;
            $history->id_status = $item->status;
            $history->id_sub_status = $item->id_sub_status;
            $history->create_date = date("Y-m-d");
            $history->enabled = 1;
            $history->save(false);
        }
    }

    public function Check_history(Policy $item)
    {

        $fecha = new DateTime();
        $f1 = $fecha->modify('first day of this month')->format('Y-m-d');
        $f2 = $fecha->modify('last day of this month')->format('Y-m-d');;
        $row = Historic::find()
            ->where(['between', 'create_date', "'" . $f1 . "'", "'" . $f2 . "'"])
            ->andWhere('id_status=' . $item->status)
            ->andWhere('id_sub_status=' . $item->id_sub_status)
            ->andWhere('id_policy=' . $item->id)
            ->one();
        if ($row != null) {
            return true;
        } else {
            return false;
        }
    }
}
