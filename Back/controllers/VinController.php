<?php

namespace app\controllers;

use app\models\Agency;
use app\models\Bank;
use app\models\Notesemployee;
use app\models\Permisions;
use app\models\NotesType;
use app\models\Notes;
use app\models\Vin;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}

class VinController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_all($id)
    {
        $items = Vin::find()->where("id_policy=" . $id)->andWhere("enabled=1")->with("policy")->asArray()->all();
        $response = [
            "status" => 'error',
            "message" => "Vins Not Found"
        ];
        if ($items != null) {
            $response = [
                "status" => 'success',
                "message" => "Vins  Found",
                "data" => $items
            ];
        }

        return $response;
    }
    public function actionGet_bank_policy($id)
    {
        $items = Bank::find()
            ->where("id_policy=" . $id)
            ->andWhere("enabled=1")
            ->with("customer")
            ->asArray()
            ->all();
        if ($items != null) {
            $response = [
                "status" => "success",
                "message" => "Cards Found.",
                "data" => $items,
            ];
        } else {
            $response = [
                "status" => "success",
                "message" => "Cards Found.",
                "data" => $items,
            ];
        }
        return $response;
    }
    public function actionGet_vin_id($id)
    {
        $item = Vin::findOne($id);
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Vin Found.",
                "data" => $item,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Vin Not Found.",
            ];
        }
        return $response;
    }
    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $note = new  Vin();
        $note->attributes = $data;
        $note->create_date = date("Y-m-d");
        if ($note->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Vin Created.",
            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "Card Not Created.",
            ];
        }

        return $response;
    }

  

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Vin::findOne($id);
        $model->attributes = $data;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Vin Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Vin Dont Updated.",
            ];
        }
        return $response;
    }
}
