<?php

namespace app\controllers;

use app\models\Agency;
use app\models\Bank;
use app\models\Notesemployee;
use app\models\Permisions;
use app\models\NotesType;
use app\models\Notes;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}

class BankController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionGet_bank_policy($id)
    {
        $items = Bank::find()
            ->where("id_policy=" . $id)
            ->andWhere("enabled=1")
            ->with("customer")
            ->asArray()
            ->all();
        if ($items != null) {
            $response = [
                "status" => "success",
                "message" => "Cards Found.",
                "data" => $items,
            ];
        } else {
            $response = [
                "status" => "success",
                "message" => "Cards Found.",
                "data" => $items,
            ];
        }
        return $response;
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $note = new  Bank();
        $note->attributes = $data;
        $note->create_date = date("Y-m-d");
        if ($note->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Card Created.",
            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "Card Not Created.",
            ];
        }

        return $response;
    }

    public function actionGet_card_id($id)
    {

        $item = Bank::findOne($id);
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Card Found.",
                "data" => $item,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Card Not Found.",
            ];
        }
        return $response;
    }

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Bank::findOne($id);
        $model->attributes = $data;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Card Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Card Dont Updated.",
            ];
        }
        return $response;
    }
    public function actionGet_types()
    {
        return NotesType::find()->where("enabled=1")->all();
    }
    public function actionSave_note()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");

        $note = new  Notes();
        $note->attributes = $data;
        $note->create_date = date("Y-m-d");
        $date =  explode("T", $note->follow_date);
        $date = Date($date[0]);
        $note->follow_date = $date;

        if ($note->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Note Created.",
            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "Note Not Created.",
            ];
        }

        return $response;
    }


    public function actionGet_notes_policy($id)
    {
        $items = Notes::find()
            ->where("id_policy=" . $id)
            ->andWhere("enabled=1")
            ->with("noteType")
            ->with("policy")
            ->with("userAsigned")
            ->with("user")
            ->asArray()
            ->orderBy(['create_date' => SORT_DESC])
            ->all();
        $response = [
            "status" => "error",
            "message" => "Notes Not Found"
        ];
        if ($items != null) {
            $response = [
                "status" => "success",
                "message" => "Notes Found",
                "data" => $items
            ];
        }
        return $response;
    }

    public function actionGet_note_by_id($id)
    {
        $item = Notes::findOne($id);
        $response = [
            "status" => "error",
            "message" => "Note Found"
        ];
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Note Not Found",
                "data" => $item
            ];
        }

        return $response;
    }

    public function actionUpdate_note()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");

        $item = Notes::findOne($id);
        $item->attributes = $data;
        $date =  explode("T", $item->follow_date);
        $date = Date($date[0]);
        $item->follow_date = $date;

        $response = [
            "status" => "error",
            "message" => "Note Can't Updated"
        ];
        if ($item->update(false)) {
            $response = [
                "status" => "success",
                "message" => "Note Updated"
            ];
        }
        return $response;
    }

    public function actionChange_notes_policy($id, $status)
    {

        $item = Notes::findOne($id);
        if ($status == 0) {
            $item->status_note = 1;
        } else {
            $item->status_note = 0;
        }
        $response = [
            "status" => 'error',
            "message" => "Error"
        ];
        if ($item->update(false)) {
            $response = [
                "status" => 'success',
                "message" => "Note Updated Successfully."
            ];
        }

        return $response;
    }
}
