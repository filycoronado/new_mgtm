<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\Customer;
use app\models\FilesPolicy;
use app\models\Policy;
use app\models\PolicyPhotos;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class PolicyController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        return Agency::find()->where("enabled=1")->all();
    }

    public function actionSave()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new  Policy();
        $model->attributes = $data;
        $model->enabled = 0;
        $model->create_date = date("Y-m-d");
        $eff = explode("T", $model->effective_date);
        $cancel_date = explode("T", $model->cancelation_date);
        $pending_date = explode("T", $model->pending_date);
        $due_date = explode("T", $model->due_date);
        $expiration_date = explode("T", $model->expiration_date);
        $model->effective_date = date($eff[0]);
        $model->cancelation_date = date($cancel_date[0]);
        $model->pending_date = date($pending_date[0]);
        $model->due_date = date($due_date[0]);
        $model->expiration_date = date($expiration_date[0]);

        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Policy Saved Successfully.",
                "policy" => $model
            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "Policy Can´t Saved.",

            ];
        }
        return $response;
    }


    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Policy::findOne($id);
        $model->create_date = date("Y-m-d");
        $eff = explode("T", $model->effective_date);
        $cancel_date = explode("T", $model->cancelation_date);
        $pending_date = explode("T", $model->pending_date);
        $due_date = explode("T", $model->due_date);
        $expiration_date = explode("T", $model->expiration_date);
        $model->effective_date = date($eff[0]);
        $model->cancelation_date = date($cancel_date[0]);
        $model->pending_date = date($pending_date[0]);
        $model->due_date = date($due_date[0]);
        $model->expiration_date = date($expiration_date[0]);
        $model->attributes = $data;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Policy Updated Successfully.",
                "customer" => $model,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Policy Dont Updated.",
            ];
        }
        return $response;
    }
    public function actionGet_by_id($id)
    {
        $policy = Policy::findOne($id);
        if ($policy != null) {
            $response = [
                "status" => "success",
                "message" => "Policy Found.",
                "data" => $policy,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Policy Not Found.",
            ];
        }
        return $response;
    }

    public function actionGet_files_by_policy($id)
    {
        $policy = FilesPolicy::find()
            ->where("id_policy=" . $id)
            ->andWhere("enabled=1")
            ->with('typeFile')
            ->with('policy')
            ->with('user')
            ->asArray()
            ->all();
        if ($policy != null) {
            $response = [
                "status" => "success",
                "message" => "Files Found.",
                "data" => $policy,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Files Not Found.",
            ];
        }
        return $response;
    }

    public function actionGet_photos_by_policy($id)
    {

        $policy = PolicyPhotos::find()
        ->where("id_policy=" . $id)
        ->andWhere("enabled=1")
        ->with('fileType')
        ->with('policy')
        ->with('user')
        ->asArray()
        ->all();
    if ($policy != null) {
        $response = [
            "status" => "success",
            "message" => "Files Found.",
            "data" => $policy,
        ];
    } else {
        $response = [
            "status" => "Error",
            "message" => "Files Not Found.",
        ];
    }
    return $response;
    }
}
