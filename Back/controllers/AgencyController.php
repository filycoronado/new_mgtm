<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class AgencyController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        return Agency::find()->where("enabled=1")->all();
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new Agency();
        $model->attributes = $data;
        $model->create_date = date("Y-m-d");
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Agency Saved Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Agency Dont Saved.",
            ];
        }
        return $response;
    }


    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id=$request->post("id");
        $model = Agency::findOne($id);
        $model->attributes = $data;
        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Agency Updated Successfully.",
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Agency Dont Updated.",
            ];
        }
        return $response;
    }
    public function actionGet_by_id($id)
    {
        $agency = Agency::findOne($id);
        if ($agency != null) {
            $response = [
                "status" => "success",
                "message" => "Agency Found.",
                "data" => $agency,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Agency Not Found.",
            ];
        }
        return $response;
    }
}
