<?php

namespace app\controllers;

use app\models\Agency;
use app\models\LlcTypes;
use app\models\PolicySubStatus;
use app\models\Statuspolicy;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class StatuspolicyController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        return Statuspolicy::find()
            ->where("enabled=1")
            ->with("policySubStatuses")
            ->asArray()
            ->all();
    }

    public function actionGet_available_sub_status($id)
    {
        $items = PolicySubStatus::find()->where("id_policystatus=" . $id)->all();
        if ($items != null) {
            $response = [
                "status" => 'success',
                'message' => "Compelte",
                'data' => $items
            ];
        } else {
            $response = [
                "status" => 'error',
                'message' => "error"
            ];
        }
        return $response;
    }
}
