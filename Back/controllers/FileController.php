<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\FilesPolicy;
use app\models\PolicyPhotos;
use phpDocumentor\Reflection\Types\Array_;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;



header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class FileController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionSave()
    {

        $message = "All Files Upload Successfully";
        $request = Yii::$app->request;
        //  return $_FILES["file0"]["name"];
        $id_policy = $request->post("id_policy");
        $length = $request->post("length");
        $id_user = $request->post("id_user");
        $target_dir = "../uploads/files/";
        $fials = array();
        $success = array();
        for ($i = 0; $i <= $length; $i++) {
            $file = "file" . $i;
            $type = "type" . $i;

            $FilesPolicy = new FilesPolicy();
            $FilesPolicy->id_policy = $id_policy;
            $FilesPolicy->id_user = $id_user;
            $FilesPolicy->id_type_file = $request->post($type);
            $FilesPolicy->create_date = date("Y-m-d");

            $check = getimagesize($_FILES[$file]["tmp_name"]);
            if ($check !== false) {
                $successUploadsError = [
                    "index" => $i,
                    "nameFile" => $_FILES[$file]["name"],
                    "name" => $file,
                ];
                array_push($fials, $successUploadsError);
                $uploadOk = 0;
            } else {
                $target_file = $target_dir . basename($_FILES[$file]["name"]);
                if (move_uploaded_file($_FILES[$file]["tmp_name"], $target_file)) {

                    $successUploads = [
                        "index" => $i,
                        "nameFile" => $_FILES[$file]["name"],
                        "name" => $file,
                    ];
                    array_push($success, $successUploads);

                    $FilesPolicy->file = $_FILES[$file]["name"];
                    $FilesPolicy->save(false);
                } else {
                    $message = "Some Files can't Upload";
                }
            }
        }
        $response = [
            "status" => 'success',
            'fails' => $fials,
            'success' => $success,
            'message' => $message
        ];
        return $response;
        //return $data;
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }


    public function actionSave_photos()
    {

        $message = "All Files Upload Successfully";
        $request = Yii::$app->request;
        //  return $_FILES["file0"]["name"];
        $id_policy = $request->post("id_policy");
        $length = $request->post("length");
        $id_user = $request->post("id_user");
        $target_dir = "../uploads/files/";
        $fials = array();
        $success = array();
        for ($i = 0; $i <= $length; $i++) {
            $file = "file" . $i;
            $type = "type" . $i;

            $FilesPolicy = new PolicyPhotos();
            $FilesPolicy->id_policy = $id_policy;
            $FilesPolicy->id_user = $id_user;
            $FilesPolicy->id_file_type = $request->post($type);
            $FilesPolicy->create_date = date("Y-m-d");

            $check = getimagesize($_FILES[$file]["tmp_name"]);
            if (!($check !== false)) {
                $successUploadsError = [
                    "index" => $i,
                    "nameFile" => $_FILES[$file]["name"],
                    "name" => $file,
                ];
                array_push($fials, $successUploadsError);
                $uploadOk = 0;
            } else {
                $target_file = $target_dir . basename($_FILES[$file]["name"]);
                if (move_uploaded_file($_FILES[$file]["tmp_name"], $target_file)) {

                    $successUploads = [
                        "index" => $i,
                        "nameFile" => $_FILES[$file]["name"],
                        "name" => $file,
                    ];
                    array_push($success, $successUploads);

                    $FilesPolicy->file = $_FILES[$file]["name"];
                    $FilesPolicy->save(false);
                } else {
                    $message = "Some Files can't Upload";
                }
            }
        }
        $response = [
            "status" => 'success',
            'fails' => $fials,
            'success' => $success,
            'message' => $message
        ];
        return $response;
        //return $data;
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}
