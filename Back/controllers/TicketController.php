<?php

namespace app\controllers;

use app\models\Agency;
use app\models\User;
use app\models\Customer;
use app\models\Historic;
use app\models\Policy;
use app\models\Tickets;
use Symfony\Component\BrowserKit\History;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\app;
use yii\filters\ContentNegotiator;

use inquid\pdf\FPDF;
use app\models\PDF;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


class TicketController extends Controller
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => ['http://localhost:4200', 'http://americadriversclub.com'],
                    'Access-Control-Request-Method' => ['POST', 'GET', 'OPTIONS', 'PUT'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actionGet_available()
    {
        return Agency::find()->where("enabled=1")->all();
    }

    public function actionSave()
    {

        $request = Yii::$app->request;
        $data = $request->post("data");
        $model = new  Tickets();
        $model->attributes = $data;
        $model->enabled = 1;
        $model->create_date = date("Y-m-d");
        $model->is_credit = 0;

        $user = User::findOne($model->id_user);
        $model->id_agency = $user->id_agency;

        if ($model->id_user_cashier == '0') {
            $model->id_user_cashier = null;
        }
        if ($model->id_dealer == '0') {
            $model->id_dealer = null;
        }
        if ($model->save(false)) {

            $policy = Policy::findOne($model->id_policy);
            $customer = Customer::findOne($model->id_customer);

            $policy->enabled = 1;
            $customer->enabled = 1;

            $policy->update(false);
            $customer->update(false);
            /**SaveHistory here  */
            $history = new Historic();
            $history->create_date = date("Y-m-d");
            $history->id_policy = $policy->id;
            $history->id_status = $policy->status;
            $history->id_sub_status = $policy->id_sub_status;

            $history->save(false);
            $response = [
                "status" => "success",
                "message" => "Ticket Saved Successfully.",

            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "Ticket Can´t Saved.",

            ];
        }
        return $response;
    }

    public function actionMake_payment()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $flag = false;
        $model = new  Tickets();
        $model->attributes = $data;
        $model->enabled = 1;
        $model->create_date = date("Y-m-d");
        $model->is_credit = 0;
        if ($model->id_user_cashier == '0') {
            $model->id_user_cashier = null;
        }
        if ($model->id_dealer == '0') {
            $model->id_dealer = null;
        }


        if (!$model->is_endorse == 1) {
            $flag = true;
            //if is not endorse change status if is required
            $policy = Policy::findOne($model->id_policy);
            $id_sub_status = $policy->id_sub_status;

            if ($id_sub_status == 3) {
                //pending cancellation to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 4) {
                //pending renewal to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 6) {
                //pending dnp to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 7) {
                //pending insure request to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 8) {
                //pending expired to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 9) {
                //pending switch to active
                $policy->status = 1;
                $policy->id_sub_status = 1;
            }

            if ($id_sub_status == 10) {
                //pending inactive to nb
                $policy->status = 1;
                $policy->id_sub_status = 2;
            }
        }


        //**move due date */
        $day = date("d", strtotime($policy->due_date));
        $new_dude_date = date("Y-m-d", strtotime($policy->due_date . "+ 1 month"));
        $month = date("m", strtotime($new_dude_date));
        $year = date("Y", strtotime($new_dude_date));

        $new_dude_date = date("Y-m-d", strtotime($year . "-" . $month . "-" . $day));
        $policy->due_date = $new_dude_date;

        //**move cancel date */
        $new_cancel_date = date("Y-m-d", strtotime($policy->cancelation_date . "+ 1 month"));
        $policy->cancelation_date = $new_cancel_date;

        //**move pendign Date */
        $pending_date = date("Y-m-d", strtotime($policy->due_date . "+ 7 days"));
        $policy->pending_date = $pending_date;

        $response = [
            'status' => 'error',
            'message' => 'Payment Error',
        ];

        if ($model->save(false)) {
            if ($policy->update(false)) {
                $response = [
                    'status' => 'success',
                    'message' => 'Payment Complete',
                ];
            }
        }
        return $response;
    }

    public function actionGet_paymet_by_policy($id)
    {
        $items =  Tickets::find()->where("id_policy=" . $id)
            ->with('policy')
            ->asArray()
            ->all();

        if ($items != null) {
            $response = [
                "status" => "success",
                "message" => "complete",
                "data" => $items
            ];
        } else {
            $response = [
                "status" => "error",
                "message" => "error",

            ];
        }

        return $response;
    }
    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = $request->post("data");
        $id = $request->post("id");
        $model = Tickets::findOne($id);
        $model->attributes = $data;
        $create_date = explode("T", $model->create_date);
        $model->create_date = date($create_date[0]);

        if ($model->save(false)) {
            $response = [
                "status" => "success",
                "message" => "Ticekt Updated Successfully.",
                "customer" => $model,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Ticekt Dont Updated.",
            ];
        }
        return $response;
    }
    public function actionGet_by_id($id)
    {
        $item = Tickets::findOne($id);
        if ($item != null) {
            $response = [
                "status" => "success",
                "message" => "Ticekt Found.",
                "data" => $item,
            ];
        } else {
            $response = [
                "status" => "Error",
                "message" => "Ticekt Not Found.",
            ];
        }
        return $response;
    }

    public function actionRender_ticket($id)
    {

        $item = Tickets::find()->where("id=" . $id)
            ->with('policy')
            ->asArray()
            ->all();
        //   return $item[0]['policy']['company']['name'];
        $create_date = date("m-d-Y", strtotime($item[0]['create_date']));
        $company = $item[0]['policy']['company']['name'];
        $customer = $item[0]['policy']['customer']['name'] . " " . $item[0]['policy']['customer']['last_name'];
        $address = $item[0]['policy']['customer']['address'];
        $state = $item[0]['policy']['customer']['state'];
        $zip = $item[0]['policy']['customer']['zipcode'];
        $total = $item[0]['insurance_amount'] + $item[0]['office_amount'] + $item[0]['adc_amount'];;
        //Yii::$app->mailer->compose('html', ['content' =>  "test"]);
        //  $gray = '161, 166, 173';
        //$black = [24, 25, 26];
        $data = ["status" => 'success'];
        $content = $this->renderPartial('@app/views/layouts/ticket', ['data' => $data]);
        $pdf = new PDF();
        $pdf->header('Content-type: application/pdf');
        $pdf->AddPage();
        $pdf->Image('../web/images/Recurso 1.PNG', 15, 20, 60);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->Text(125, 25, "INVOICE");
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Text(125, 30, "Office :");
        $pdf->Text(125, 35, "Phone:");
        $pdf->Text(125, 40, "Fax:");


        $pdf->SetTextColor(161, 166, 173);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Text(140, 30, "505 W AJO WAY TUCSON AZ 85713");
        $pdf->Text(138, 35, "(520)-807-0619");
        $pdf->Text(133, 40, "(520)-807-0619");


        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setFillColor(239, 239, 239);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetXY(20, 55);
        $pdf->Cell(60, 8, "Invoice to: " . $customer, 0, 1, 'C', 1); //your cell

        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Text(20, 72, $address . ',' . $state . ',' . $zip);



        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Text(140, 72, "Invoice # ");
        $pdf->Text(140, 76, "Date ");

        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Text(160, 72, $item[0]['id']);
        $pdf->Text(150, 76, $create_date);

        /**Totals */

        $pdf->SetTextColor(59, 61, 70);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Text(130, 145, "SUBTOTAL");
        $pdf->Text(130, 150, "TAX(%0)");

        $pdf->SetTextColor(59, 61, 70);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Text(155, 145, "$" . $total);
        $pdf->Text(155, 150, "$0");

        /**TARGET  CARD FOOTER */

        $pdf->SetFont('Arial', 'B', 13);
        $pdf->setFillColor(239, 239, 239);
        $pdf->SetTextColor(59, 61, 70);
        $pdf->SetXY(20, 180);
        $pdf->Cell(80, 8, "PAYMENT METHOD (CASH)", 0, 1, 'C', 1); //your cell
        $pdf->SetXY(110, 180);
        $pdf->Cell(80, 8, "GRAND TOTAL $" . $total, 0, 1, 'C', 1); //your cell



        $pdf->SetFont('Arial', '', 10);
        //$pdf->setFillColor(255, 255, 239);
        $pdf->SetTextColor(59, 61, 70);
        $pdf->SetXY(20, 190);
        $pdf->MultiCell(80, 5, "Effective  1 july lorem insput lorem inpunr Effective  1 july lorem insput lorem inpunr", 0,  'C', 0); //your cell
        $pdf->SetXY(20, 205);
        $pdf->MultiCell(80, 5, "Effective  1 july lorem insput lorem inpunr Effective  1 july lorem insput lorem inpunr", 0,  'C', 0); //your cell


        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Text(110, 195, $company);
        $pdf->Text(110, 200, "DATE" . $create_date);
        $pdf->Text(110, 205, "POLICY #" . $item[0]['policy']['policy_number']);

        /***Bottomom separator */
        $pdf->SetFont('Arial', 'B', 13);
        $pdf->setFillColor(239, 239, 239);
        $pdf->SetTextColor(59, 61, 70);
        $pdf->SetXY(20, 225);
        $pdf->Cell(80, 1, "", 0, 1, 'C', 1); //your cell
        $pdf->SetXY(110, 225);
        $pdf->Cell(80, 1, "", 0, 1, 'C', 1); //your cell


        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Text(25, 230, "Thank you for your business");
        $pdf->Text(120, 230, $customer);


        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setFillColor(59, 61, 70);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->SetXY(13, 85);
        $pdf->Cell(60, 12, "DESCRIPTION", 0, 0, 'C', 1); //your cell
        $pdf->Cell(60, 12, "DATE", 0, 0, 'C', 1);
        $pdf->Cell(60, 12, "AMOUNT", 0, 0, 'C', 1);
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 8);
        $pdf->setFillColor(255, 255, 255);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetX(13);
        $pdf->Cell(60, 8, "Payment", 0, 0, 'C', 1); //your cell
        $pdf->Cell(60, 8, $create_date, 0, 0, 'C', 1);
        $pdf->Cell(60, 8, "$ " . $total, 0, 0, 'C', 1);

        $pdf->ln();
        $pdf->setFillColor(239, 239, 239);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetX(13);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1); //your cell
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);
        $pdf->ln();
        $pdf->setFillColor(255, 255, 255);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetX(13);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1); //your cell
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);
        $pdf->ln();
        $pdf->setFillColor(239, 239, 239);
        $pdf->SetTextColor(24, 25, 26);
        $pdf->SetX(13);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1); //your cell
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);
        $pdf->Cell(60, 8, "", 0, 0, 'C', 1);






        // $pdf->WriteHTML($content);
        $pdf->Output();
        exit;
    }
}
